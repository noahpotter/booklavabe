# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('architecture', '0008_auto_20151118_0934'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClubMembership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='club',
            name='members',
        ),
        migrations.AddField(
            model_name='club',
            name='members',
            field=models.ManyToManyField(related_name='subscribed_clubs', through='architecture.ClubMembership', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='clubmembership',
            name='club',
            field=models.ForeignKey(to='architecture.Club'),
        ),
        migrations.AddField(
            model_name='clubmembership',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
