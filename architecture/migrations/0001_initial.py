# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Club',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('club_name', models.CharField(max_length=30)),
                ('club_description', models.TextField(max_length=320)),
                ('club_members', models.ManyToManyField(related_name='members', to=settings.AUTH_USER_MODEL)),
                ('club_president', models.ForeignKey(related_name='president', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
