# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0005_auto_20151113_1622'),
    ]

    operations = [
        migrations.CreateModel(
            name='Discussion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('index', models.IntegerField()),
                ('start_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(max_length=2000)),
                ('nestedLevel', models.IntegerField()),
                ('parentPost', models.ForeignKey(related_name='child_posts', to='architecture.Post')),
            ],
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('originalPost', models.ForeignKey(related_name='threads', to='architecture.Discussion')),
            ],
        ),
        migrations.AlterField(
            model_name='book',
            name='description',
            field=models.TextField(max_length=2000),
        ),
        migrations.AlterField(
            model_name='clubbook',
            name='book',
            field=models.ForeignKey(related_name='club_books', to='architecture.Book'),
        ),
        migrations.AlterField(
            model_name='clubbook',
            name='club',
            field=models.ForeignKey(related_name='club_books', to='architecture.Club'),
        ),
        migrations.AddField(
            model_name='post',
            name='thread',
            field=models.ForeignKey(related_name='posts', to='architecture.Thread'),
        ),
        migrations.AddField(
            model_name='discussion',
            name='clubBook',
            field=models.ForeignKey(related_name='discussions', to='architecture.ClubBook'),
        ),
    ]
