# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0003_club_creation_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=578)),
                ('description', models.TextField(max_length=600)),
                ('author', models.CharField(max_length=80)),
            ],
        ),
        migrations.CreateModel(
            name='ClubBook',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date', models.DateTimeField(auto_now_add=True)),
                ('end_date', models.DateTimeField()),
                ('book', models.OneToOneField(to='architecture.Book')),
                ('club', models.OneToOneField(to='architecture.Club')),
            ],
        ),
    ]
