# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-29 21:06
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0016_auto_20151222_0237'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClubBookDiscussion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('start_chapter', models.IntegerField(default=1)),
                ('end_chapter', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='ClubDiscussion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('club', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='discussions', to='architecture.Club')),
            ],
        ),
        migrations.RemoveField(
            model_name='discussion',
            name='club_book',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='discussion',
        ),
        migrations.AlterField(
            model_name='clubbook',
            name='start_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.DeleteModel(
            name='Discussion',
        ),
        migrations.AddField(
            model_name='clubbookdiscussion',
            name='club_book',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='discussions', to='architecture.ClubBook'),
        ),
        migrations.AddField(
            model_name='thread',
            name='club_book_discussion',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='threads', to='architecture.ClubBookDiscussion'),
        ),
        migrations.AddField(
            model_name='thread',
            name='club_discussion',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='threads', to='architecture.ClubDiscussion'),
        ),
    ]
