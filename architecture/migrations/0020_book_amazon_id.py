# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-03 22:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0019_auto_20160303_2134'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='amazon_id',
            field=models.CharField(default=None, max_length=100, null=True, unique=True),
        ),
    ]
