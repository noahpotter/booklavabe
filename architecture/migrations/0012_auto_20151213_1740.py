# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0011_auto_20151213_0352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='parent_post',
            field=models.ForeignKey(related_name='child_posts', default=None, to='architecture.Post', null=True),
        ),
    ]
