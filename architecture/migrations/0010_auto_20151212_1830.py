# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0009_auto_20151128_1829'),
    ]

    operations = [
        migrations.RenameField(
            model_name='discussion',
            old_name='clubBook',
            new_name='club_book',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='nestedLevel',
            new_name='nested_level',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='parentPost',
            new_name='parent_post',
        ),
        migrations.RenameField(
            model_name='thread',
            old_name='originalPost',
            new_name='original_post',
        ),
        migrations.AlterUniqueTogether(
            name='clubmembership',
            unique_together=set([('club', 'user')]),
        ),
    ]
