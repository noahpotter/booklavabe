# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='club',
            old_name='club_description',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='club',
            old_name='club_members',
            new_name='members',
        ),
        migrations.RenameField(
            model_name='club',
            old_name='club_name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='club',
            old_name='club_president',
            new_name='president',
        ),
    ]
