# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0002_auto_20151016_0024'),
    ]

    operations = [
        migrations.AddField(
            model_name='club',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 10, 16, 4, 31, 58, 392000, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
