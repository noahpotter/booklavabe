from django.db import models
from django.contrib.auth.models import User, Group
from django.utils import timezone
import requests
from django.core.files.base import ContentFile
from guardian.shortcuts import get_perms

from django.conf import settings
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from descriptors import AutoOneToOneField
from django.core.exceptions import ObjectDoesNotExist

from django.core.validators import MinValueValidator

from django.db.models.signals import post_delete, post_save, pre_migrate
from guardian.shortcuts import assign_perm, remove_perm

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import models as auth_models
from django.contrib.auth.models import Permission

# custom user related permissions
# @receiver(pre_migrate, sender=auth_models)
# Need to get this to work and take out from post_save_user
def add_user_permissions(sender, **kwargs):
    content_type = ContentType.objects.get_for_model(settings.AUTH_USER_MODEL)
    Permission.objects.get_or_create(codename='view_user', name='View user', content_type=content_type)
    Permission.objects.get_or_create(codename='add_club', name='Add club', content_type=content_type)

pre_migrate.connect(add_user_permissions, auth_models)

def post_save_user(sender, instance, created, raw, using, update_fields, **kwargs):
    if created and instance.username != settings.ANONYMOUS_USER_NAME:
        # Fixtures aren't loaded at this point yet so check to see if this group exists
        try:
            group = Group.objects.get(name='user')
        except ObjectDoesNotExist:
            group = Group.objects.create(name='user')
            content_type = ContentType.objects.get_for_model(User)
            Permission.objects.get_or_create(codename='view_user', name='View user', content_type=content_type)
            Permission.objects.get_or_create(codename='add_club', name='Add club', content_type=content_type)
            Permission.objects.get_or_create(codename='view_club', name='View club', content_type=content_type)
            Permission.objects.get_or_create(codename='unsubscribe_club', name='Unubscribe from club', content_type=content_type)
            Permission.objects.get_or_create(codename='change_userdetail', name='Change user detail', content_type=content_type)
        
        assign_perm('auth.view_user', group, instance)
        assign_perm('auth.add_club', group, instance)
        assign_perm('auth.view_club', instance, instance)
        assign_perm('auth.unsubscribe_club', instance, instance)
        instance.groups.add(group)

        assign_perm('auth.change_userdetail', instance, instance.user_detail)

    elif created:
        # This is the anonymous user
        try:
            group = Group.objects.get(name='anon_user')
        except ObjectDoesNotExist:
            group = Group.objects.create(name='anon_user')
            # content_type = ContentType.objects.get_for_model(User)
            # Permission.objects.get_or_create(codename='view_club', name='View club', content_type=content_type)
            # content_type = ContentType.objects.get_for_model(Club)
            # Permission.objects.get_or_create(codename='view_club', name='View club', content_type=content_type)

        instance.groups.add(group)

def post_delete_user(sender, instance, using, **kwargs):
    pass

post_save.connect(post_save_user, User)
post_delete.connect(post_delete_user, User)

def user_subscription_or_none(self):
    try:
        return self.user_subscription
    except:
        return None

User.add_to_class("user_subscription_or_none", user_subscription_or_none)

# class ModelName():
#     def get_model_name(self):
#         if self.has_attr('model_name'):
#             return self.model_name
#         else:
#             return super(ModelName, self).get_model_name()

class UserDetail(models.Model):
    model_name = 'user_detail'

    user = AutoOneToOneField(User, related_name='user_detail')
    password_reset_token = models.CharField(max_length=100, default='')
    image = models.ImageField(upload_to='users', max_length=300, null=True)

# Create your models here.
class Club(models.Model):
    model_name = 'club'

    president = models.ForeignKey(User, related_name='owned_clubs')
    name = models.CharField(max_length=30)
    description = models.TextField(max_length=1000)
    members = models.ManyToManyField(User, related_name='subscribed_clubs', through='ClubMembership')
    creation_date = models.DateTimeField(auto_now_add=True)
    use_club_image = models.BooleanField(default=False)
    image = models.ImageField(upload_to='clubs', max_length=300, null=True)

    class Meta:
        permissions = (
            ('add_post', 'Add post'),
            ('view_post', 'View post'),
            ('add_thread', 'Add thread'),
            ('view_thread', 'View thread'),
            ('view_clubdiscussion', 'View club discussion'),
            ('view_clubbook', 'View club book'),
            ('add_clubbook', 'Add club book'),
            ('view_club', 'View club'),
            ('view_clubbookdiscussion', 'View club book discussion'),
            ('add_clubbookdiscussion', 'Add club book discussion'),
        )

    def _get_discussion(self):
        if self.discussions.count() > 0:
            return self.discussions.first()
        else:
            return None

    discussion = property(_get_discussion)

def post_save_club(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        group = Group.objects.create(name='club_members_%s' % instance.id)
        assign_perm('add_post', group, instance)
        assign_perm('view_post', group, instance)
        assign_perm('add_thread', group, instance)
        assign_perm('view_thread', group, instance)
        assign_perm('view_clubdiscussion', group, instance)
        assign_perm('view_clubbookdiscussion', group, instance)

        group = Group.objects.get(name='user')
        assign_perm('add_club', group, instance)
        assign_perm('view_club', group, instance)
        assign_perm('view_clubbook', group, instance)
        
        group = Group.objects.get(name='anon_user')
        assign_perm('view_club', group, instance)

        assign_perm('add_clubbook', instance.president, instance)
        assign_perm('add_clubbookdiscussion', instance.president, instance)

        assign_perm('change_club', instance.president, instance)
        assign_perm('delete_club', instance.president, instance)

def post_delete_club(sender, instance, using, **kwargs):
    Group.objects.get(name='club_members_%s' % instance.id).delete()

post_save.connect(post_save_club, Club)
post_delete.connect(post_delete_club, Club)

class ClubMembership(models.Model):
    model_name = 'club_membership'

    club = models.ForeignKey(Club)
    user = models.ForeignKey(User)

    class Meta:
        unique_together = ('club', 'user')

def post_save_club_membership(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        group = Group.objects.get(name='club_members_%s' % instance.club.id)
        instance.user.groups.add(group)

def post_delete_club_membership(sender, instance, using, **kwargs):
    group = Group.objects.get(name='club_members_%s' % instance.club.id)
    instance.user.groups.remove(group)

post_save.connect(post_save_club_membership, ClubMembership)
post_delete.connect(post_delete_club_membership, ClubMembership)

class Book(models.Model):
    model_name = 'book'

    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1500, null=True)
    author = models.CharField(max_length=80, null=True)
    cover_image = models.ImageField(upload_to='books', max_length=300, null=True)
    amazon_id = models.CharField(max_length=100, default=None, unique=True, null=True)
    amazon_link = models.CharField(max_length=300, default='', null=True)

    # def get_cover_image(self):
    #     r = requests.get('url')
    #     file_content = ContentFile(r.content)
    #     self.cover_image.save('name.png|jpg', file_content)
    #     self.save()
    
class ClubBook(models.Model):
    model_name = 'club_book'

    book = models.ForeignKey(Book, related_name='club_books')
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField()
    club = models.ForeignKey(Club, related_name='club_books')

    def __unicode__(self):
        return u'%s' % (self.book.name)

    class Meta:
        permissions = (
            ('view_clubbook', 'View club book'),
            ('view_clubbookdiscussion', 'View club book discussion'),
            ('add_clubbookdiscussion', 'Add club book discussion'),
        )

def post_save_club_book(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        group = Group.objects.get(name='club_members_%s' % instance.club.id)
        assign_perm('view_clubbookdiscussion', group, instance)

        assign_perm('add_clubbookdiscussion', instance.club.president, instance)
        assign_perm('change_clubbook', instance.club.president, instance)
        assign_perm('delete_clubbook', instance.club.president, instance)

        group = Group.objects.get(name='user')
        assign_perm('view_clubbook', group, instance)

def post_delete_club_book(sender, instance, using, **kwargs):
    pass

post_save.connect(post_save_club_book, ClubBook)
post_delete.connect(post_delete_club_book, ClubBook)

class ClubBookDiscussion(models.Model):
    model_name = 'club_book_discussion'

    club_book = models.ForeignKey(ClubBook, related_name='discussions')
    start_date = models.DateTimeField(default=timezone.now)
    start_chapter = models.IntegerField(validators=[MinValueValidator(1)])
    end_chapter = models.IntegerField(validators=[MinValueValidator(1)])
    
    class Meta:
        permissions = (
            ('view_clubbookdiscussion', 'View club book discussion'),
        )

    def _get_club(self):
        if self.club_book and self.club_book.club:
            return self.club_book.club
        else:
            return None

    club = property(_get_club)

def post_save_club_book_discussion(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        club_members_group = Group.objects.get(name='club_members_%s' % instance.club.id)
        assign_perm('view_clubbookdiscussion', club_members_group, instance)
        assign_perm('change_clubbookdiscussion', instance.club.president, instance)
        assign_perm('delete_clubbookdiscussion', instance.club.president, instance)

def post_delete_club_book_discussion(sender, instance, using, **kwargs):
    club_members_group = Group.objects.get(name='club_members_%s' % instance.club.id)
    remove_perm('view_clubbookdiscussion', club_members_group, instance)

post_save.connect(post_save_club_book_discussion, ClubBookDiscussion)
post_delete.connect(post_delete_club_book_discussion, ClubBookDiscussion)

class ClubDiscussion(models.Model):
    model_name = 'club_discussion'

    club = models.ForeignKey(Club, related_name='discussions')
    date = models.DateTimeField(default=timezone.now)
    
    class Meta:
        permissions = (
            ('view_clubdiscussion', 'View club discussion'),
        )

def post_save_club_discussion(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        club_members_group = Group.objects.get(name='club_members_%s' % instance.club.id)
        assign_perm('view_clubdiscussion', club_members_group, instance)

def post_delete_club_discussion(sender, instance, using, **kwargs):
    club_members_group = Group.objects.get(name='club_members_%s' % instance.club.id)
    remove_perm('view_clubdiscussion', club_members_group, instance)

post_save.connect(post_save_club_discussion, ClubDiscussion)
post_delete.connect(post_delete_club_discussion, ClubDiscussion)

class Thread(models.Model):
    model_name = 'thread'

    club_book_discussion = models.ForeignKey(ClubBookDiscussion, related_name='threads', null=True,  default=None)
    club_discussion = models.ForeignKey(ClubDiscussion, related_name='threads', null=True,  default=None)
    date = models.DateTimeField(default=timezone.now)

    class Meta:
        permissions = (
            ('view_thread', 'View thread'),
        )

    def _get_original_post(self):
        for post in self.posts.all():
            if post.parent_post is None:
                return post
        return None

    original_post = property(_get_original_post)

    def _get_club(self):
        if self.club_book_discussion:
            return self.club_book_discussion.club
        elif self.club_discussion:
            return self.club_discussion.club
        else:
            return None

    club = property(_get_club)

def post_save_thread(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        club_members_group = Group.objects.get(name='club_members_%s' % instance.club.id)
        assign_perm('view_thread', club_members_group, instance)

def post_delete_thread(sender, instance, using, **kwargs):
    pass
    # There's an error when trying to access instance.club and one of the objects
    # between this thread and the club is already deleted, as happens when deleting say the club_book
    # club_members_group = Group.objects.get(name='club_members_%s' % instance.club.id)
    # remove_perm('view_thread', club_members_group, instance)

post_save.connect(post_save_thread, Thread)
post_delete.connect(post_delete_thread, Thread)

class Post(models.Model):
    model_name = 'post'

    thread = models.ForeignKey(Thread, related_name='posts', null=True, default=None)
    user = models.ForeignKey(User, related_name='posts', null=True, default=None)
    parent_post = models.ForeignKey('self', related_name='child_posts', null=True, default=None)
    content = models.TextField(max_length=1000)
    nested_level = models.IntegerField(default=0)
    date = models.DateTimeField(default=timezone.now)
    deleted = models.BooleanField(default=False)

    # If more than report_max reports are submitted, this post is flagged as deleted
    MAX_REPORTS = 5

    class Meta:
        permissions = (
            ('view_post', 'View post'),
            ('report_post', 'Report post'),
        )

def post_save_post(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        assign_perm('delete_post', instance.user, instance)
        assign_perm('change_post', instance.user, instance)

        club_members_group = Group.objects.get(name='club_members_%s' % instance.thread.club.id)
        assign_perm('view_post', club_members_group, instance)
        assign_perm('report_post', club_members_group, instance)

def post_delete_post(sender, instance, using, **kwargs):
    remove_perm('delete_post', instance.user, instance)
    remove_perm('change_post', instance.user, instance)
    remove_perm('view_post', instance.user, instance)

post_save.connect(post_save_post, Post)
post_delete.connect(post_delete_post, Post)

class Subscription(models.Model):
    model_name = 'subscription'

    name = models.CharField(max_length=100, unique=True)
    max_owned_clubs = models.IntegerField(default=0)
    max_subscribed_clubs = models.IntegerField(default=0)

    # Change to use fixtures
    @staticmethod
    def create_subscriptions():
        try:
            subscription = Subscription.objects.get(name='default')
            subscription.delete()
        except ObjectDoesNotExist:
            pass

        try:
            subscription = Subscription.objects.get(name='subscription_1')
            subscription.delete()
        except ObjectDoesNotExist:
            pass

        try:
            subscription = Subscription.objects.get(name='subscription_unlimited')
            subscription.delete()
        except ObjectDoesNotExist:
            pass

        subscription = Subscription(
                name='default',
                max_owned_clubs=2,
                max_subscribed_clubs=3
            )
        subscription.save()

        subscription = Subscription(
                name='subscription_1',
                max_owned_clubs=4,
                max_subscribed_clubs=8
            )
        subscription.save()

        subscription = Subscription(
                name='subscription_unlimited',
                max_owned_clubs=1000,
                max_subscribed_clubs=1000
            )
        subscription.save()

class UserSubscription(models.Model):
    model_name = 'user_subscription'

    user = models.OneToOneField(User, related_name='user_subscription', null=True, default=None)
    subscription = models.ForeignKey(Subscription, related_name='user_subscriptions', null=True, default=None)

class PostReport(models.Model):
    model_name = 'post_report'

    post = models.ForeignKey(Post, related_name='reports', default=None)
    user = models.ForeignKey(User, related_name='reported_posts', default=None)

    class Meta:
        unique_together = ('post', 'user')

