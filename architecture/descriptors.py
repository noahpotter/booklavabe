from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.fields.related_descriptors import (
    ReverseOneToOneDescriptor,
)

class AutoReverseOneToOneDescriptor(ReverseOneToOneDescriptor):
    def __get__(self, instance, type=None):
        try:
            return super(AutoReverseOneToOneDescriptor, self).__get__(instance, type)
        except self.RelatedObjectDoesNotExist:
            kwargs = {
                self.related.field.name: instance,
            }
            rel_obj = self.related.related_model._default_manager.create(**kwargs)
            setattr(instance, self.cache_name, rel_obj)
            return rel_obj

class AutoOneToOneField(models.OneToOneField):
    related_accessor_class = AutoReverseOneToOneDescriptor