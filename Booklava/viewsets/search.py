from django.contrib.auth.models import User
from architecture.models import Club, Book
from Booklava.serializers import PublicUserSerializer, BookSerializer, ClubSerializer

from rest_framework.mixins import ListModelMixin 
from rest_framework.viewsets import GenericViewSet

from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.files.base import ContentFile
from Booklava.permissions import SearchPermission

from django.db.models import Q

import requests
from amazon.api import AmazonAPI, SearchException

ASSOCIATES_ID = 'booklava-20'
amazon = AmazonAPI('AKIAJD5SUOP5L3EW2ZDQ', 'vr93CzBLKnyCmCj8fSHUv7MWHGxIkHDzvx4Vrq0P', ASSOCIATES_ID)

class SearchViewSet(ListModelMixin, GenericViewSet):
    permission_classes = (SearchPermission,)
    search_serializers = {
        'books': BookSerializer,
        'users': PublicUserSerializer,
        'clubs': ClubSerializer
    }

    def get_queryset(self):
        query_type = self.request.query_params.get('type')
        query = self.request.query_params.get('query', None)
        page = int(self.request.query_params.get('page', 1));
        result = Q()

        if query is not None and query is not u'':
            if query_type == 'clubs':
                queryset = Club.objects.all()
                parts = query.split(' ')
                if len(parts) > 0 and parts[0] == 'book:':
                    result = result | Q(club_books__book__name__iexact=' '.join(parts[1:]))
                else:
                    for part in parts:
                        result = result | Q(name__icontains=part) | Q(description__icontains=part)

                paginator = Paginator(queryset.filter(result), 10) # Show 10 results per page

                try:
                    results = paginator.page(page)
                except PageNotAnInteger:
                    # If page is not an integer, deliver first page.
                    results = paginator.page(1)
                except EmptyPage:
                    # If page is out of range (e.g. 9999), deliver last page of results.
                    results = None

                return results

            elif query_type == 'users':
                queryset = User.objects.all()
                parts = query.split(' ')
                for part in parts:
                    result = result | Q(username__icontains=part)

            elif query_type == 'books':
                books = amazon.search(Keywords=query, ResponseGroup='EditorialReview,Images,ItemAttributes', SearchIndex='Books')
                books.current_page = page
                queryset = Book.objects.all()
                found_books = False

                num_books = 10
                current_book = 0
                try:
                    for i, product in enumerate(books):
                        found_books = True
                        # print product.editorial_reviews
                        book = None
                        try:
                            book = Book.objects.get(amazon_id=product.asin)
                            result = result | Q(amazon_id=product.asin)
                            current_book += 1
                        except ObjectDoesNotExist:
                            image_url = product.medium_image_url or product.small_image_url or  product.large_image_url
                            image_content = None
                            if image_url:
                                image_content = ContentFile(requests.get(image_url).content) 
                            # http://www.amazon.com/dp/ASIN/?tag=your_Associates_ID
                            serializer = BookSerializer(data={'amazon_id': product.asin, 'name': product.title, 'description': product.editorial_review[:1500], 'author': product.author, 'amazon_link': 'http://www.amazon.com/dp/%s/?tag=%s' % (product.asin, ASSOCIATES_ID)})
                            is_valid = serializer.is_valid(raise_exception=True)
                            if is_valid:
                                serializer.save()

                                if image_url:
                                    serializer.instance.cover_image.save(image_url, image_content)

                                result = result | Q(amazon_id=product.asin)

                                current_book += 1
                            else:
                                print 'is not valid', serializer

                        if current_book >= num_books:
                            break
                except SearchException:
                    pass

                if found_books:
                    return queryset.filter(result)
                else:
                    return None


            return queryset.filter(result)


        return None

    def get_serializer_class(self):
        query_type = self.request.query_params.get('type')
        return self.search_serializers[query_type]
