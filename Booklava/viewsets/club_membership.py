from architecture.models import Club, ClubMembership
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet, CreateListViewSet
from rest_framework.mixins import CreateModelMixin
from rest_framework import status
from rest_framework.response import Response
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name
from rest_framework.decorators import detail_route
from Booklava.permissions import SubscribeToClubPermission
from Booklava.serializers import ClubMembershipSerializer, ClubSerializer

class ClubMembershipViewSet(CreateModelMixin, RetrieveUpdateDestroyViewSet):
    serializer_class = ClubMembershipSerializer
    queryset =  ClubMembership.objects.all()

    additional_permission_classes = (SubscribeToClubPermission, )

    # @detail_route(methods=['delete'])
    # def unsubscribe(self, request, *args, **kwargs):
    #     user_id = self.request.query_params.get('user_id', None)
    #     club_id = self.request.query_params.get('club_id', None)
    #     club_membership = ClubMembership.objects.get(user_id=user_id, club_id=club_id)
    #     club_membership.delete()
        
    #     try:
    #         book = Book.objects.get(pk=request.query_params.get('book'))
    #         club_books = book.club_books.filter(book__pk=book.pk)

    #         for club_book in club_books:
    #             if club_book.start_date < timezone.now() and timezone.now() < club_book.end_date:
    #                 return Response({'response':  True})

    #     except ObjectDoesNotExist:
    #         return Response({'response': False})
    #     return Response({'response': False})
