from architecture.models import Thread, Post
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet, CreateListViewSet
from rest_framework.response import Response
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name
from Booklava.serializers import ThreadSerializer, PostSerializer

class ThreadViewSet(RetrieveUpdateDestroyViewSet):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer

class ThreadPostViewSet(CreateListViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    parent_serializer_class = ThreadSerializer

    def get_parent_permission_obj(self, kwargs):
        thread = self.get_parent_obj(kwargs)
        return thread.club

    def list(self, request, *args, **kwargs):
        queryset = self.queryset
        has_posts = self.request.query_params.getlist('has_posts', None)
        thread_id = kwargs[compose_parent_pk_kwarg_name('thread')]

        if has_posts is not None and thread_id is not None:
            queryset = queryset.filter(thread_id=thread_id).exclude(id__in=has_posts)

            serializer = PostSerializer(queryset, context={'request': request}, many=True)
        else:
            serializer = PostSerializer(Post.objects.none(), context={'request': request}, many=True)

        return Response(serializer.data)
