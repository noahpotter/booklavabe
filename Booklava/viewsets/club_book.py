from architecture.models import ClubBook, ClubBookDiscussion
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet, CreateListViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from Booklava.serializers import ClubBookDiscussionSerializer, ClubBookSerializer
from Booklava.filters import ClubBookFilter

from rest_framework import filters

# Club Book
class ClubBookViewSet(RetrieveUpdateDestroyViewSet):
    queryset = ClubBook.objects.all()
    serializer_class = ClubBookSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClubBookFilter

class ClubBookClubBookDiscussionViewSet(CreateListViewSet):
    queryset = ClubBookDiscussion.objects.all()
    serializer_class = ClubBookDiscussionSerializer
    parent_serializer_class = ClubBookSerializer

    def get_parent_permission_obj(self, kwargs):
        club_book = self.get_parent_obj(kwargs)
        return club_book.club