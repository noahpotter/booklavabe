from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework.viewsets import GenericViewSet
import json
from rest_framework.permissions import IsAuthenticated

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
class FeedbackViewSet(GenericViewSet):
    queryset = None
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return None

    @list_route(methods=['post'])
    def feedback(self, request):
        message = request.data.get('message')
        stores = json.dumps(request.data.get('stores'), sort_keys=True, indent=4)
        time = request.data.get('time')
        
        plaintext = get_template('feedback.txt')
        htmltext = get_template('feedback.html')
        data = { 'message': message, 'stores': stores, 'time': time }

        subject, from_email, to = 'Booklava - Feedback', 'booklava@outlook.com', 'noah.potter@outlook.com'
        text_content = plaintext.render(data)
        html_content = htmltext.render(data)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        # msg.attach_alternative(html_content, 'text/html')
        msg.send()

        return Response({'success': 'Email sent.'}, status=status.HTTP_201_CREATED)
