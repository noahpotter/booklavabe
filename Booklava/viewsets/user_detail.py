from architecture.models import UserDetail
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet
from Booklava.serializers import UserDetailSerializer

class UserDetailViewSet(RetrieveUpdateDestroyViewSet):
    queryset = UserDetail.objects.all()
    serializer_class = UserDetailSerializer
