from rest_framework.authtoken import views
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework.authtoken.models import Token
from Booklava.serializers import AuthTokenSerializer

class ObtainAuthToken(views.ObtainAuthToken):
    serializer_class = AuthTokenSerializer

    # Copied from https://github.com/tomchristie/django-rest-framework/blob/6284bceaaff0e53349131164ce5c16cda8deb715/rest_framework/authtoken/views.py
    # Wanted to add id field
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'id': user.id})
    
    @list_route(methods=['delete'])
    def delete(self, request):
        Token.objects.filter(user=request.user).delete()
        return Response({'success': 'success'})