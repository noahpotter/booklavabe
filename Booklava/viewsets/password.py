from django.contrib.auth.models import User
from architecture.models import UserDetail
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from Booklava.permissions import PasswordPermission
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework.viewsets import GenericViewSet
from django.core.exceptions import ObjectDoesNotExist
from Booklava.serializers import UserSerializer
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template

class PasswordViewSet(GenericViewSet):
    queryset = User.objects.none()
    serializer_class = UserSerializer
    permission_classes = (PasswordPermission,)

    @list_route(methods=['post'])
    def forgot(self, request):
        email = request.data.get('email')
        try:
            user = User.objects.get(email=email)
        except ObjectDoesNotExist:
            return Response({'email': ['Email does not exist.']}, status=status.HTTP_400_BAD_REQUEST) 

        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(user)
        user.user_detail.password_reset_token = token
        user.user_detail.save()
        
        plaintext = get_template('forgot_password.txt')
        htmltext = get_template('forgot_password.html')
        data = { 'url': self.request.data.get('domain') + '/passwords/reset?token=' + token }

        subject, from_email, to = 'Booklava - Reset Password', 'booklava@outlook.com', self.request.data.get('email')
        text_content = plaintext.render(data)
        html_content = htmltext.render(data)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()

        return Response({'success': 'Email sent.'}, status=status.HTTP_201_CREATED)

    @list_route(methods=['post'])
    def reset(self, request):
        token = self.request.data.get('token')

        try:
            user = UserDetail.objects.get(password_reset_token=token).user
        except ObjectDoesNotExist:
            return Response({'token': ['Expired token.']}, status=status.HTTP_400_BAD_REQUEST) 

        serializer = self.serializer_class(user, data={'password': self.request.data.get('password')}, partial=True, context={'request': request, 'reset_password': True})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        serializer.instance.user_detail.password_reset_token = ''
        serializer.instance.user_detail.save()
        return Response(serializer.data)
