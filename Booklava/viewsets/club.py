from architecture.models import Club, ClubDiscussion, ClubBook, Book
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet, CreateListViewSet
from rest_framework.response import Response
from rest_framework.decorators import detail_route, permission_classes
from django.core.exceptions import ObjectDoesNotExist
from Booklava.serializers import ClubSerializer, ClubDiscussionSerializer, ClubBookSerializer
from Booklava.filters import ClubBookFilter

from rest_framework import filters

from django.utils import timezone

class ClubViewSet(RetrieveUpdateDestroyViewSet):
    queryset = Club.objects.all()
    serializer_class = ClubSerializer

    @detail_route(methods=['get'])
    def reading_book(self, request, *args, **kwargs):
        club = self.get_object()
        try:
            book = Book.objects.get(pk=request.query_params.get('book'))
            club_books = book.club_books.filter(book__pk=book.pk)

            for club_book in club_books:
                if club_book.start_date < timezone.now() and timezone.now() < club_book.end_date:
                    return Response({'response':  True})

        except ObjectDoesNotExist:
            return Response({'response': False})
        return Response({'response': False})

class ClubClubDiscussionViewSet(CreateListViewSet):
    queryset = ClubDiscussion.objects.all()
    serializer_class = ClubDiscussionSerializer
    parent_serializer_class = ClubSerializer

class ClubClubBookViewSet(CreateListViewSet):
    queryset = ClubBook.objects.all()
    serializer_class = ClubBookSerializer
    parent_serializer_class = ClubSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClubBookFilter
