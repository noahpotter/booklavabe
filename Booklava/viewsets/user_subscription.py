from rest_framework.viewsets import ModelViewSet
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet
from Booklava.serializers import UserSubscriptionSerializer
from architecture.models import UserSubscription

class UserSubscriptionViewSet(RetrieveUpdateDestroyViewSet):
    queryset = UserSubscription.objects.all()
    serializer_class = UserSubscriptionSerializer
