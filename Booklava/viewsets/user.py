from django.contrib.auth.models import User
from Booklava.viewsets.custom_viewsets import ModelViewSet, CreateListViewSet
from architecture.models import Club, ClubMembership
from rest_framework.response import Response
from Booklava.serializers import UserSerializer, PublicUserSerializer, ClubSerializer
from Booklava.permissions import CreateClubPermission, UserClubPermission
from rest_framework import decorators
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name
from rest_framework.decorators import detail_route, list_route
from rest_framework import status
from rest_framework.viewsets import GenericViewSet
from django.http import Http404
from django.core.exceptions import ObjectDoesNotExist

class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    # serializer_class = UserSerializer
    permission_classes = ()

    def get_serializer_class(self):
        if self.action == 'retrieve' and str(self.request.user.id) != self.kwargs['pk']:
            return PublicUserSerializer
        else:
            return UserSerializer

class UserClubViewSet(CreateListViewSet):
    queryset = Club.objects.all()
    serializer_class = ClubSerializer
    parent_serializer_class = UserSerializer
    permission_classes = (CreateClubPermission, )
    # additional_permission_classes = (UserClubPermission, CreateClubPermission)

    def get_parent_model_name(self):
        return 'president'

    # Test this
    def create(self, request, *args, **kwargs):
        parent_obj = self.get_parent_obj(kwargs)

        data = request.data.copy()
        # data['%s_id' % self.get_parent_model_name()] = parent_obj.id

        serializer = self.serializer_class(data=data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        obj = self.get_parent_obj(kwargs)

        queryset = queryset.filter(**{'%s__id' % self.get_parent_model_name(): obj.id})
        queryset = obj.subscribed_clubs
        serializer = self.serializer_class(queryset, context={'request': request}, many=True)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        # We can assume this object exists since it made it past permission checking
        parent_obj = User.objects.get(id = kwargs[compose_parent_pk_kwarg_name('president')])

        try:
            obj = Club.objects.get(id = kwargs['pk'])
        except ObjectDoesNotExist:
            raise Http404

        try:
            club_membership = ClubMembership.objects.get(user=parent_obj, club=obj)
        except ObjectDoesNotExist:
            print 'no'
            raise Http404

        club_membership.delete()
        return Response({'success': 'success'}, status=status.HTTP_200_OK)