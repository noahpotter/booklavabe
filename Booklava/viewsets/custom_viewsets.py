from Booklava.permissions import ListPermission
from django.http import Http404

from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, ListModelMixin 
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response
from rest_framework import status, filters
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name

from django.core.exceptions import ObjectDoesNotExist

from rest_framework_extensions.mixins import NestedViewSetMixin

class DestroyModelMixin(object):
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        print instance.__dict__
        self.perform_destroy(instance)
        return Response({'success': 'success'}, status=status.HTTP_200_OK)

    def perform_destroy(self, instance):
        instance.delete()

class ModelViewSet(CreateModelMixin, RetrieveModelMixin, ListModelMixin, UpdateModelMixin, DestroyModelMixin, GenericViewSet):
    additional_permission_classes = ()
    _ignore_model_permissions = True

    def get_permissions(self):
        return [permission() for permission in self.additional_permission_classes + tuple(self.permission_classes)]

class RetrieveUpdateDestroyViewSet(RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin, GenericViewSet):
    additional_permission_classes = ()
    _ignore_model_permissions = True

    def get_permissions(self):
        return [permission() for permission in self.additional_permission_classes + tuple(self.permission_classes)]

class CreateListViewSet(NestedViewSetMixin, CreateModelMixin, ListModelMixin, GenericViewSet):
    # filter_backends = (filters.DjangoObjectPermissionsFilter,)
    additional_permission_classes = (ListPermission,)
    parent_serializer_class = None
    _ignore_model_permissions = True

    def get_parent_model_name(self):
        if hasattr(self.parent_serializer_class.Meta.model, 'model_name'):
            return self.parent_serializer_class.Meta.model.model_name
        else:
            return self.parent_serializer_class.Meta.model._meta.model_name

    # Might need to change urls to use modelnames without underscores
    def get_parent_obj(self, kwargs):
        return self.parent_serializer_class.Meta.model.objects.get(id = kwargs[compose_parent_pk_kwarg_name(self.get_parent_model_name())])

    def get_parent_permission_obj(self, kwargs):
        return self.get_parent_obj(kwargs)

    def get_object(self):
        if self.request.method == 'POST' or self.request.method == 'GET' or self.request.method == 'DELETE':
            try:
                return self.get_parent_permission_obj(self.kwargs)
            except ObjectDoesNotExist:
                raise Http404
        else:
            return super(CreateListViewSet, self).get_object()

    def create(self, request, *args, **kwargs):
        self.check_permissions(request)
        parent_obj = self.get_parent_obj(kwargs)

        data = request.data.copy()
        data['%s_id' % self.get_parent_model_name()] = parent_obj.id

        serializer = self.serializer_class(data=data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        obj = self.get_parent_obj(kwargs)

        queryset = queryset.filter(**{'%s__id' % self.get_parent_model_name(): obj.id})
        serializer = self.serializer_class(queryset, context={'request': request}, many=True)
        return Response(serializer.data)

    def get_permissions(self):
        # for permission in self.additional_permission_classes + tuple(self.permission_classes):
        #     print permission

        return [permission() for permission in (ListPermission, ) + self.additional_permission_classes + tuple(self.permission_classes)]

    def check_permissions(self, request):
        obj = self.get_object()
        self.check_object_permissions(request, obj)
