from architecture.models import ClubBookDiscussion, Thread
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet, CreateListViewSet
from rest_framework import status
from rest_framework.response import Response
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name
from django.core.exceptions import ObjectDoesNotExist
from Booklava.serializers import ThreadSerializer, ClubBookDiscussionSerializer
from django.http import Http404

class ClubBookDiscussionViewSet(RetrieveUpdateDestroyViewSet):
    queryset = ClubBookDiscussion.objects.all()
    serializer_class = ClubBookDiscussionSerializer

class ClubBookDiscussionThreadViewSet(CreateListViewSet):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer
    parent_serializer_class = ClubBookDiscussionSerializer

    def get_parent_permission_obj(self, kwargs):
        club_book_discussion = self.get_parent_obj(kwargs)
        return club_book_discussion.club

    def list(self, request, *args, **kwargs):
        queryset = self.queryset
        has_threads = self.request.query_params.getlist('has_threads', None)
        club_book_discussion_id = kwargs[compose_parent_pk_kwarg_name('club_book_discussion')]

        if has_threads is not None and club_book_discussion_id is not None:
            queryset = queryset.filter(club_book_discussion_id=club_book_discussion_id).exclude(id__in=has_threads)
            serializer = ThreadSerializer(queryset, context={'request': request}, many=True)
            return Response(serializer.data)

        serializer = ThreadSerializer(Thread.objects.none(), context={'request': request}, many=True)
        return Response(serializer.data)
