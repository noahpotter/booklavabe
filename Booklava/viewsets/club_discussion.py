from architecture.models import ClubDiscussion, Thread
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet, CreateListViewSet
from rest_framework.response import Response
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name
from Booklava.serializers import ThreadSerializer, ClubDiscussionSerializer

class ClubDiscussionViewSet(RetrieveUpdateDestroyViewSet):
    queryset = ClubDiscussion.objects.all()
    serializer_class = ClubDiscussionSerializer

class ClubDiscussionThreadViewSet(CreateListViewSet):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer
    parent_serializer_class = ClubDiscussionSerializer

    def get_parent_permission_obj(self, kwargs):
        club_discussion = self.get_parent_obj(kwargs)
        return club_discussion.club

    def list(self, request, *args, **kwargs):
        queryset = self.queryset
        has_threads = self.request.query_params.getlist('has_threads', None)
        club_discussion_id = kwargs[compose_parent_pk_kwarg_name('club_discussion')]

        if has_threads is not None and club_discussion_id is not None:
            queryset = queryset.filter(club_discussion_id=club_discussion_id).exclude(id__in=has_threads)
            serializer = ThreadSerializer(queryset, context={'request': request}, many=True)
            return Response(serializer.data)

        serializer = ThreadSerializer(Thread.objects.none(), context={'request': request}, many=True)
        return Response(serializer.data)
