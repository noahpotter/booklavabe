from architecture.models import Book
from Booklava.viewsets.custom_viewsets import ModelViewSet
from Booklava.serializers import BookSerializer

class BookViewSet(ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
