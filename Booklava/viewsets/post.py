from architecture.models import Post, PostReport
from Booklava.viewsets.custom_viewsets import RetrieveUpdateDestroyViewSet
from rest_framework import status
from rest_framework.authtoken import views
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from Booklava.permissions import ReportPostPermission
from Booklava.serializers import PostSerializer

class PostViewSet(RetrieveUpdateDestroyViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    # May still need to create a custom DjangoObjectPermission class that defaults to ignoring model permissions but still enforces them if that permission has been set by the user
    # Or just set model permissions that but seems more difficult

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.deleted = True
        instance.save()
        serializer = PostSerializer(instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    @detail_route(methods=['post'], permission_classes=[ReportPostPermission])
    def report(self, request, *args, **kwargs):
        post = self.get_object()
        user = request.user

        if not user.reported_posts.filter(post=post).exists():
            post_report = PostReport(user=user, post=post)
            post_report.save()

        if post.reports.count() > Post.MAX_REPORTS:
            post.deleted = True
            post.save()
            
        serializer = PostSerializer(post, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)
