from Booklava.viewsets.custom_viewsets import ModelViewSet
from Booklava.serializers import SubscriptionSerializer

class SubscriptionViewSet(ModelViewSet):
    serializer_class = SubscriptionSerializer
