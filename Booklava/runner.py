from Booklava.media_runner import TempMediaMixin
from django.test.runner import DiscoverRunner

class CustomTestSuiteRunner(TempMediaMixin, DiscoverRunner):
    "Local test suite runner."