from django.conf.urls import include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedSimpleRouter

from Booklava.viewsets.auth import ObtainAuthToken
from Booklava.viewsets.book import BookViewSet
from Booklava.viewsets.club import ClubViewSet ,ClubClubDiscussionViewSet ,ClubClubBookViewSet
from Booklava.viewsets.club_book import ClubBookViewSet, ClubBookClubBookDiscussionViewSet
from Booklava.viewsets.club_book_discussion import ClubBookDiscussionViewSet, ClubBookDiscussionThreadViewSet
from Booklava.viewsets.club_discussion import ClubDiscussionViewSet, ClubDiscussionThreadViewSet
from Booklava.viewsets.club_membership import ClubMembershipViewSet
from Booklava.viewsets.feedback import FeedbackViewSet
from Booklava.viewsets.password import PasswordViewSet
from Booklava.viewsets.post import PostViewSet
from Booklava.viewsets.search import SearchViewSet
from Booklava.viewsets.subscription import SubscriptionViewSet
from Booklava.viewsets.thread import ThreadViewSet, ThreadPostViewSet
from Booklava.viewsets.user import UserViewSet, UserClubViewSet
from Booklava.viewsets.user_detail import UserDetailViewSet
from Booklava.viewsets.user_subscription import UserSubscriptionViewSet

from Booklava import settings
from django.conf.urls.static import static

router = ExtendedSimpleRouter()

router.register(r'books', BookViewSet, base_name='book')
router.register(r'user_details', UserDetailViewSet, base_name='user_detail')
router.register(r'search', SearchViewSet, base_name='search')

# /users
# /users/:id/clubs
users_routes = router.register( r'users', UserViewSet, base_name='user')
users_routes.register(          r'clubs',
                                    UserClubViewSet,
                                    base_name='users-club',
                                    parents_query_lookups=['president'])

# /clubs
# /clubs/:id/club_books
# /clubs/:id/discussions
clubs_routes = router.register( r'clubs', ClubViewSet, base_name='club')
clubs_routes.register(          r'club_books',
                                    ClubClubBookViewSet,
                                    base_name='clubs-club_book',
                                    parents_query_lookups=['club'])
clubs_routes.register(          r'discussions',
                                    ClubClubDiscussionViewSet,
                                    base_name='clubs-club_discussion',
                                    parents_query_lookups=['club'])

# /club_books
# /club_books/:id/discussions
club_books_routes = router.register(r'club_books', ClubBookViewSet, base_name='clubbook')
club_books_routes.register(         r'discussions',
                                        ClubBookClubBookDiscussionViewSet,
                                        base_name='club_books-club_discussion',
                                        parents_query_lookups=['club_book'])

# /club_memberships
club_memberships_routes = router.register(r'club_memberships', ClubMembershipViewSet, base_name='clubmembership')

# /club_book_discussions
# /club_book_discussions/:id/threads
cb_discussions_routes = router.register( r'club_book_discussions', ClubBookDiscussionViewSet, base_name='clubbookdiscussion')
cb_discussions_routes.register(          r'threads',
                                            ClubBookDiscussionThreadViewSet,
                                            base_name='club_book_discussions-thread',
                                            parents_query_lookups=['club_book_discussion'])

# /club_discussions
# /club_discussions/:id/threads
c_discussions_routes = router.register( r'club_discussions', ClubDiscussionViewSet, base_name='clubdiscussion')
c_discussions_routes.register(          r'threads',
                                            ClubDiscussionThreadViewSet,
                                            base_name='club_discussions-thread',
                                            parents_query_lookups=['club_discussion'])

# /threads
# /threads/:id/posts
threads_routes = router.register(   r'threads', ThreadViewSet, base_name='thread')
threads_routes.register(            r'posts',
                                        ThreadPostViewSet,
                                        base_name='threads-post',
                                        parents_query_lookups=['thread'])

router.register(r'posts', PostViewSet, base_name='post')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^passwords/forgot/', PasswordViewSet.as_view({'post': 'forgot'})),
    url(r'^passwords/reset/', PasswordViewSet.as_view({'post': 'reset'})),
    url(r'^feedback/', FeedbackViewSet.as_view({'post': 'feedback'})),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-token-auth/', ObtainAuthToken.as_view())
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
