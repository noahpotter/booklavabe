from django.contrib.auth.models import User
from architecture.models import (
    UserDetail,
    Club,
    ClubMembership,
    Book,
    ClubBook,
    ClubBookDiscussion,
    ClubDiscussion,
    Thread,
    Post,
    PostReport,
    Subscription,
    UserSubscription
)
import json
from rest_framework import serializers, exceptions
from rest_framework.validators import UniqueValidator, UniqueTogetherValidator
from django.contrib.auth.hashers import make_password
from datetime import datetime
from django.contrib.auth.models import Group

from guardian.shortcuts import assign_perm

from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from django.core.exceptions import ObjectDoesNotExist

class UserDetailSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    password = serializers.CharField(write_only=True, required=False, min_length=5, error_messages={
            "blank": "Password cannot be empty.",
            "min_length": "Password too short.",
        })

    class Meta:
        model = UserDetail
        fields = ('id', 'image', 'user', 'password')

    def update(self, instance, validated_data):
        if 'password' in validated_data and instance.user.check_password(validated_data['password']):
            super(UserDetailSerializer, self).update(instance, validated_data)
            return instance
        raise exceptions.ValidationError({'password': ['Password is invalid.']})

class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True, required=False, min_length=5, error_messages={
            "blank": "Password cannot be empty.",
            "min_length": "Password too short.",
        })
    new_password = serializers.CharField(write_only=True, required=False, allow_blank=True, min_length=5, error_messages={
            "min_length": "Password too short.",
        })
    user_detail = UserDetailSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'url', 'user_detail', 'username', 'email', 'password', 'new_password', 'owned_clubs', 'subscribed_clubs')
        read_only_fields = ('user_detail', 'is_staff', 'owned_clubs', 'subscribed_clubs')

    def create(self, validated_data):
        try:
            default_subscription = Subscription.objects.get(name='default')
        except ObjectDoesNotExist:
            Subscription.create_subscriptions()
            default_subscription = Subscription.objects.get(name='default')

        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
        )
        user.set_password(validated_data['password'])
        user.save()
        
        user.user_subscription = UserSubscription(
            subscription=default_subscription
        )
        user.user_subscription.save()
        return user

    def update(self, instance, validated_data):
        if 'reset_password' in self.context and self.context['reset_password']:
            instance.set_password(validated_data['password'])
            instance.save()
            return instance

        if 'password' in validated_data and instance.check_password(validated_data['password']):
            if 'new_password' in validated_data and validated_data['new_password'] != '':
                instance.set_password(validated_data['new_password'])
                validated_data.pop('new_password', None)
                instance.save()

            validated_data.pop('password', None)
            super(UserSerializer, self).update(instance, validated_data)
            # instance.save()
            return instance
        raise exceptions.ValidationError({'password': ['Password is invalid.']})

    # def validate_password(self, value):
    #     return make_password(value)

class PublicUserSerializer(UserSerializer):
    class Meta(UserSerializer.Meta):
        fields = ('id', 'url', 'user_detail', 'username', 'password', 'new_password', 'owned_clubs', 'subscribed_clubs')

class BookSerializer(serializers.ModelSerializer):
    description = serializers.CharField(max_length=1500)

    class Meta:
        model = Book
        fields = ('id', 'name', 'url', 'description', 'author', 'cover_image', 'amazon_id', 'amazon_link')

    def validate_name(self, value):
        return value[:200]

    def validate_description(self, value):
        if len(value) < 100:
            return False
        else:
            return value[:1500]

class ClubSerializer(serializers.ModelSerializer):
    president = UserSerializer(read_only=True)
    members = UserSerializer(many=True, read_only=True)
    description = serializers.CharField(allow_blank=True, default='', max_length=1000)
    use_club_image = serializers.BooleanField(default=True)
    discussions = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    creation_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Club
        fields = ('id', 'url', 'name', 'president', 'members', 'creation_date', 'description', 'use_club_image', 'image', 'discussions')

    def create(self, validated_data):
        user = None
        if hasattr(self.context['request'], 'user'):
            user = self.context['request'].user
        else:
            user = self.context['request']['user']

        if not hasattr(validated_data, 'image'):
            validated_data['image'] = None

        club = Club(
            name=validated_data['name'],
            description=validated_data['description'],
            use_club_image=validated_data['use_club_image'],
            image=validated_data['image'],
            president=user
        )
        club.save()
        ClubDiscussion.objects.create(club=club)
        ClubMembership.objects.create(club=club, user=user)
        return club

class ClubMembershipSerializer(serializers.ModelSerializer):
    club = ClubSerializer(read_only=True, default=None)
    user = UserSerializer(read_only=True, default=None)
    club_id = serializers.PrimaryKeyRelatedField(queryset=Club.objects.all(), source='club', write_only=True)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='user', write_only=True)

    class Meta:
        model = ClubMembership
        fields = ('id', 'club', 'user', 'club_id', 'user_id')
        validators = [
            UniqueTogetherValidator(
                queryset=ClubMembership.objects.all(),
                fields=('club', 'user'),
                message='{{"unique_together": "User is already a member of this club."}}'
            )
        ]

class ClubBookSerializer(serializers.ModelSerializer):
    club = ClubSerializer(read_only=True)
    book = BookSerializer(read_only=True)
    club_id = serializers.PrimaryKeyRelatedField(queryset=Club.objects.all(), source='club', write_only=True)
    book_id = serializers.PrimaryKeyRelatedField(queryset=Book.objects.all(), source='book', write_only=True)
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()

    def validate(self, data):
        if data['start_date'] > data['end_date']:
            raise serializers.ValidationError({'start_date': 'Start must be before end date.'})
        elif data['start_date'] < timezone.make_aware(datetime(2015, 1, 1), timezone.get_default_timezone()):
            raise serializers.ValidationError({'start_date': 'Start must be before 1/1/2015.', })
        return data

    class Meta:
        model = ClubBook
        fields = ('id', 'book', 'url', 'start_date','end_date','club', 'club_id', 'book_id')

class ClubBookDiscussionSerializer(serializers.ModelSerializer):
    club_book = ClubBookSerializer(read_only=True)
    club_book_id = serializers.PrimaryKeyRelatedField(queryset=ClubBook.objects.all(), source='club_book', write_only=True)

    class Meta:
        model = ClubBookDiscussion
        fields = ('id', 'club_book', 'club_book_id', 'url', 'start_date', 'start_chapter', 'end_chapter')

    def validate(self, data):
        if data['start_chapter'] > data['end_chapter']:
            raise serializers.ValidationError({'start_chapter': 'Ensure Start Chapter is less than or equal to End Chapter'})
        return data

class ClubDiscussionSerializer(serializers.ModelSerializer):
    club = ClubSerializer(read_only=True)
    club_id = serializers.PrimaryKeyRelatedField(queryset=Club.objects.all(), source='club', write_only=True)
    date = serializers.DateTimeField()

    class Meta:
        model = ClubDiscussion
        fields = ('id', 'club', 'club_id', 'date')
        
class PostSerializer(serializers.ModelSerializer):
    thread = serializers.PrimaryKeyRelatedField(read_only=True)
    thread_id = serializers.PrimaryKeyRelatedField(queryset=Thread.objects.all(), source='thread', write_only=True)
    parent_post = serializers.PrimaryKeyRelatedField(read_only=True)
    parent_post_id = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all(), source='parent_post', write_only=True, required=False, default=None)
    content = serializers.CharField(min_length=1, max_length=1000)
    nested_level = serializers.IntegerField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    date = serializers.DateTimeField(read_only=True)
    deleted = serializers.BooleanField(read_only=True)
    # content = serializers.SerializerMethodField()
    # content = serializers.ModelField(model_field=Post()._meta.get_field('content'))
    # content = PostContentField

    class Meta:
        model = Post
        fields = ('id', 'thread', 'thread_id', 'url', 'parent_post', 'parent_post_id', 'content', 'nested_level', 'user', 'date', 'deleted')

    def create(self, validated_data):
        if hasattr(self.context['request'], 'user'):
            user = self.context['request'].user
        else:
            user = self.context['request']['user']

        validated_data['user'] = user

        if 'parent_post' in validated_data and validated_data['parent_post'] is not None:
            validated_data['nested_level'] = validated_data['parent_post'].nested_level + 1
        else:
            validated_data['nested_level'] = 0

        post = Post(
            **validated_data
        )
        post.save()
        return post

    def to_representation(self, instance):
        ret = super(PostSerializer, self).to_representation(instance)
        
        if ret['deleted']:
            ret['content'] = ''

        return ret
        
class PostReportSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True, default=None)
    post = PostSerializer(read_only=True, default=None)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='user', write_only=True)
    post_id = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all(), source='post', write_only=True)

    class Meta:
        model = PostReport
        fields = ('id', 'user', 'post', 'user_id', 'post_id')

class ThreadSerializer(serializers.ModelSerializer):
    club_book_discussion = ClubBookDiscussionSerializer(read_only=True)
    club_book_discussion_id = serializers.PrimaryKeyRelatedField(queryset=ClubBookDiscussion.objects.all(), source='club_book_discussion', write_only=True, required=False)
    club_discussion = ClubDiscussionSerializer(read_only=True)
    club_discussion_id = serializers.PrimaryKeyRelatedField(queryset=ClubDiscussion.objects.all(), source='club_discussion', write_only=True, required=False)
    content = serializers.CharField(max_length=1000, write_only=True, required=False)
    original_post = PostSerializer(read_only=True)
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True, required=False)
    date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Thread
        fields = ('id', 'club_book_discussion', 'club_book_discussion_id', 'club_discussion', 'club_discussion_id', 'original_post', 'content', 'url', 'user', 'date')

    def create(self, validated_data):
        
        if 'content' in validated_data:

            if hasattr(self.context['request'], 'user'):
                user = self.context['request'].user
            else:
                user = self.context['request']['user']

            thread = Thread(
                club_book_discussion=validated_data.get('club_book_discussion'),
                club_discussion=validated_data.get('club_discussion'))
            thread.save()
            post = thread.posts.create(content=validated_data['content'], user=user, nested_level=0)
            post.save()
            return thread
        else:
            thread = Thread(**validated_data)
            thread.save()
            return thread

class SubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscription
        fields = ('id', 'name', 'max_owned_clubs', 'max_subscribed_clubs')

class UserSubscriptionSerializer(serializers.ModelSerializer):
    subscription = SubscriptionSerializer(read_only=True)

    class Meta:
        model = UserSubscription
        fields = ('id', 'subscription')

# Pulled from the github page in order to allow logging in with email
class AuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField(style={'input_type': 'password'})

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = None
            try:
                user = User.objects.get(email=email)
            except ObjectDoesNotExist:
                raise exceptions.ValidationError({'email': 'Email is invalid.'})

            if user:
                authenticated_user = authenticate(username=user.username, password=password)

                if authenticated_user:
                    if not authenticated_user.is_active:
                        msg = _('User account is disabled.')
                        raise exceptions.ValidationError({'email': 'User account is disabled'})
                else:
                    msg = _('Unable to log in with provided credentials.')
                    raise exceptions.ValidationError({'email': 'Unable to log in with provided credentials.'})
        else:
            msg = _('Must include "email" and "password".')
            raise exceptions.ValidationError({'email': 'Must include "email" and "password".'})

        attrs['user'] = user
        return attrs