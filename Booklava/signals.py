# from django.db.models.signals import post_save
# from django.contrib.auth.models import User
# from architecture.models import UserDetail

# def create_detail(sender, **kw):
#     user = kw["instance"]
#     if kw["created"]:
#         detail = UserDetail(user=user)
#         detail.save()

# post_save.connect(create_detail, sender=User, dispatch_uid="users-detailcreation-signal")

from django.apps import AppConfig
from django.db import models
from django.db.models.signals import post_migrate

# Add permissions to groups
# http://unclearthoughts.net/posts/58
# admin is just a dump from [p.codename for p in Permission.objects.all()] for now
group_perms = {
  u'admin': {
    'perms': (u'add_logentry',)
  },
  u'user': {
    'perms': ()
  },
  u'guest': {
    'perms': ()
  },
  u'club_member': {
    'perms': (u'view_post',)
  }
}

def update_group_permissions(sender, **kwargs):
  from django.contrib.auth.models import Group
  from django.contrib.auth.models import Permission

  if unicode(sender).find("django.contrib.auth.models") != -1:
    for group_name in group_perms.keys():
      print 'adding permissions for {0}'.format(group_name)
      group = Group.objects.get(name=group_name)
      for perm_codename in group_perms[group_name]['perms']:
        perm = Permission.objects.get(codename=perm_codename)
        group.permissions.add(perm)
      group.save()

post_migrate.connect(update_group_permissions)