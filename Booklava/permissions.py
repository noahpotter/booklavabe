from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from django.http import Http404
from guardian.shortcuts import get_perms, get_group_perms, get_users_with_perms
from django.conf import settings
import json

class CreateClubPermission(permissions.BasePermission):
    message = 'You have reached your owned club limit.'

    def has_object_permission(self, request, view, user):
        if (request.method == 'POST'):
            if user.user_subscription_or_none() is None:
                return False
                error = json.dumps({'no_subscription': 'You don\'t have a subscription.'})
                raise PermissionDenied(error)
            else:
                max_club_count = user.user_subscription.subscription.max_owned_clubs
                allowed = len(user.owned_clubs.all()) < max_club_count

                if allowed:
                    return True
                else:
                    if max_club_count == 1:
                        clubs_str = 'club'
                    else:
                        clubs_str = 'clubs'

                    error = json.dumps({"owned_club_limit_reached": 'You\'ve reached your limit of %s owned %s.' % (max_club_count, clubs_str)})
                    raise PermissionDenied(error)
        else: 
            return True

class SubscribeToClubPermission(permissions.BasePermission):
    message = 'You have reached your subscribed club limit.'

    def has_permission(self, request, view):
        if (request.method == 'POST'):
            if request.user.user_subscription_or_none() is None:
                error = json.dumps({'no_subscription': 'You don\'t have a subscription.'})
                raise PermissionDenied(error)
            else:
                # This probably shouldn't work like this, but subscribed_clubs is a list of all clubs a user is apart of since owning a club kinda means you're subscribed to it too...
                max_club_count = request.user.user_subscription.subscription.max_subscribed_clubs
                allowed = len(request.user.subscribed_clubs.all()) - len(request.user.owned_clubs.all()) < max_club_count
                if allowed:
                    return True
                else:
                    if max_club_count == 1:
                        clubs_str = 'club'
                    else:
                        clubs_str = 'clubs'

                    error = json.dumps({"subscribed_club_limit_reached": 'You\'ve reached your limit of %s subscribed %s.' % (max_club_count, clubs_str)})
                    raise PermissionDenied(error)
        else:
            return True

SAFE_METHODS = ('GET', 'HEAD', 'OPTIONS')

class DjangoObjectPermissions(permissions.DjangoObjectPermissions):
    perms_map = {
        # 'GET': ['%(app_label)s.view_%(model_name)s'],
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }
    # Problem here. Make it so that if creating using parent/:id/:children,
    # permissions check view_parent instead of view_children so that we
    # get a 403 instead of a 404

    def get_required_object_permissions(self, method, app_label_model_cls, model_name_model_cls):
        kwargs = {
            'app_label': app_label_model_cls._meta.app_label,
            'model_name': model_name_model_cls._meta.model_name
        }
        return [perm % kwargs for perm in self.perms_map[method]]

    def has_object_permission(self, request, view, obj):
        if hasattr(view, 'get_queryset'):
            queryset = view.get_queryset()
        else:
            queryset = getattr(view, 'queryset', None)

        assert queryset is not None, (
            'Cannot apply DjangoObjectPermissions on a view that '
            'does not set `.queryset` or have a `.get_queryset()` method.'
        )

        # if request.method == 'POST' and view.parent_serializer_class is not None:
        #     print obj.__class__
        #     app_label_model_cls = obj.__class__
        #     model_name_model_cls = queryset.model
        # else:
        #     app_label_model_cls = queryset.model
        #     model_name_model_cls = queryset.model
            
        app_label_model_cls = obj.__class__
        model_name_model_cls = queryset.model
        perms = self.get_required_object_permissions(request.method, app_label_model_cls, model_name_model_cls)
        user = request.user

        # print perms, user, obj
        if not user.has_perms(perms, obj):
            # If the user does not have permissions we need to determine if
            # they have read permissions to see 403, or not, and simply see
            # a 404 response.

            if request.method == 'POST' and hasattr(view, 'parent_serializer_class') and view.parent_serializer_class is not None:
                model_name_model_cls = view.parent_serializer_class.Meta.model
                perms = self.get_required_object_permissions('GET' , app_label_model_cls, model_name_model_cls)
                # print perms, user, obj
                if not user.has_perms(perms, obj):
                    raise Http404
            else:
                if request.method in SAFE_METHODS:
                    # Read permissions already checked and failed, no need
                    # to make another lookup.
                    raise Http404

                read_perms = self.get_required_object_permissions('GET', app_label_model_cls, model_name_model_cls)
                if not user.has_perms(read_perms, obj):
                    raise Http404

            # Has read permissions.
            return False

        return True

class ReportPostPermission(DjangoObjectPermissions):
    message = 'You can\'t report your own post.'
    perms_map = {
        'POST': ['%(app_label)s.report_%(model_name)s'],
        'GET': ['%(app_label)s.view_%(model_name)s'],
    }

    def has_permission(self, request, view):
        obj = view.get_object()
        return request.user.id != obj.user.id

class UserClubPermission(DjangoObjectPermissions):
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['auth.unsubscribe_club'],
    }

class ListPermission(permissions.BasePermission):
    message = 'You don\t have access to this route.'

    def has_permission(self, request, view):
        if (request.method == 'GET'):
            obj = view.get_object()
            kwargs = {
                'app_label': view.queryset.model._meta.app_label,
                'model_name': view.queryset.model._meta.model_name
            }
            if not request.user.has_perm('%(app_label)s.view_%(model_name)s' % kwargs, obj):
                raise Http404
            return True
        else:
            return True

class SearchPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.pk != None

class PasswordPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.pk == None

