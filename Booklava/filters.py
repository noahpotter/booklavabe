from architecture.models import (
    UserDetail,
    Club,
    ClubMembership,
    Book,
    ClubBook,
    ClubBookDiscussion,
    ClubDiscussion,
    Thread,
    Post,
    UserDetail
)

import django_filters
from django_filters.filterset import STRICTNESS

class ClubBookFilter(django_filters.FilterSet):
    class Meta:
        model = ClubBook
        fields = {
            'start_date': ['lt', 'gt', 'lte', 'gte'],
            'end_date': ['lt', 'gt', 'lte', 'gte'],
            'book__name': ['iexact']
        }
    strict = STRICTNESS.RAISE_VALIDATION_ERROR

class BookFilter(django_filters.FilterSet):
    pass

