from rest_framework.test import APITestCase
from architecture.models import ClubMembership
from django.contrib.auth.models import User
from architecture.models import Club
from rest_framework import status
from tests.utils import TestHelpers
from Booklava.serializers import ClubMembershipSerializer
from django.utils import timezone
from datetime import datetime, timedelta

class TestQueries(APITestCase):
    # These aren't very necessary because the dev could get club_books using users/id/club_books?
    def test_is_currently_reading_book_true(self):
        club = TestHelpers.create_club()
        book = TestHelpers.create_book()
        club_book = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book.id, 'end_date': timezone.now() + timedelta(days=1)})
        response = self.client.get('/clubs/%s/reading_book/' % club.id, {'book': book.id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['response'])

    def test_is_currently_reading_book_false(self):
        club = TestHelpers.create_club()
        book = TestHelpers.create_book()
        club_book = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book.id, 'start_date': timezone.now() - timedelta(days=2), 'end_date': timezone.now() - timedelta(days=1)})
        response = self.client.get('/clubs/%s/reading_book/' % club.id, {'book': book.id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(response.data['response'])

    def test_filtering_club_books_on_book_name(self):
        pass

    def test_filtering_club_books_between_date(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.subscribe_users_to_club([logged_user], club)
        book = TestHelpers.create_book()
        now = timezone.now().replace(microsecond=0)
        now_str = now.strftime("%Y-%m-%d %H:%M:%S")
        club_book = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book.id, 'start_date': now, 'end_date': now + timedelta(days=1)})
        response = self.client.get('/clubs/%s/club_books/' % club.id, {'start_date__lte': now_str, 'end_date__gte': now_str}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_filtering_club_books_by_book_name_between_date(self):
        book_name = 'mybookname'
        now = timezone.now().replace(microsecond=0)
        now_str = now.strftime("%Y-%m-%d %H:%M:%S")

        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.subscribe_users_to_club([logged_user], club)
        book_1 = TestHelpers.create_book()
        book_2 = TestHelpers.create_book({'name': book_name})
        club_book_1 = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book_1.id, 'start_date': now, 'end_date': now + timedelta(days=1)})
        club_book_2 = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book_2.id, 'start_date': now, 'end_date': now + timedelta(days=1)})

        response = self.client.get('/clubs/%s/club_books/' % club.id, {'book__name__iexact': book_name, 'start_date__lte': now_str, 'end_date__gte': now_str}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_filtering_club_books_outside_date(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.subscribe_users_to_club([logged_user], club)
        book = TestHelpers.create_book()
        now = timezone.now().replace(microsecond=0)
        now_str = now.strftime("%Y-%m-%d %H:%M:%S")
        club_book = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book.id, 'start_date': now - timedelta(days=2), 'end_date': now - timedelta(days=1)})
        response = self.client.get('/clubs/%s/club_books/' % club.id, {'start_date__lte': now_str, 'end_date__gte': now_str}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

class OnCreation(APITestCase):
    def test_creates_club_discussion(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.post('/users/%s/clubs/' % user.id, {'name': 'test', 'description': 'test description'}, format='json')
        self.assertEqual(len(response.data['discussions']), 1)

    def test_fields(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.post('/users/%s/clubs/' % user.id, {'name': 'test', 'description': 'test description'}, format='json')
        self.assertEqual(response.data['name'], 'test')
        self.assertEqual(response.data['president']['username'], user.username)
        self.assertEqual(response.data['president']['id'], user.id)
        self.assertEqual(response.data['members'][0]['username'], user.username)
        self.assertEqual(response.data['members'][0]['id'], user.id)

class AuthorizedActionTestCases(APITestCase):

    def test_user_create_club(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.post('/users/%s/clubs/' % user.id, {'name': 'test', 'description': 'test description'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_president_list_own_clubs(self):
        logged_user = TestHelpers.login_user(self)['user']
        stranger_user = TestHelpers.create_user()
        stranger_club = TestHelpers.create_club({'president': stranger_user})
        club = TestHelpers.create_club({'president': logged_user})
        response = self.client.get('/users/%s/clubs/' % logged_user.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_member_list_own_clubs(self):
        logged_user = TestHelpers.login_user(self)['user']
        stranger_user = TestHelpers.create_user()
        stranger_club = TestHelpers.create_club({'president': stranger_user})
        club = TestHelpers.create_club()
        TestHelpers.subscribe_users_to_club([logged_user], club)
        response = self.client.get('/users/%s/clubs/' % logged_user.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_non_member_list_own_clubs(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        response = self.client.get('/users/%s/clubs/' % logged_user.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_president_update_club(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club({'president': logged_user})
        response = self.client.patch('/clubs/%s/' % club.id, {'name': 'new club name'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_president_delete_club(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club({'president': logged_user})
        response = self.client.delete('/clubs/%s/' % club.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # Non CRUD

    def test_owner_remove_subscribed_club(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.subscribe_users_to_club([logged_user], club)
        self.assertEqual(logged_user.subscribed_clubs.count(), 1)
        response = self.client.delete('/users/%s/clubs/%s/' % (logged_user.id, club.id), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(logged_user.subscribed_clubs.count(), 0)


class UnauthorizedActionTestCases(APITestCase):

    def test_non_user_create_club(self):
        self.client.logout()
        response = self.client.post('/users/1/clubs/', {'name': 'new name'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_list_stranger_clubs(self):
        logged_user = TestHelpers.login_user(self)['user']
        stranger_user = TestHelpers.create_user()
        stranger_club = TestHelpers.create_club({'president': stranger_user})
        club = TestHelpers.create_club()
        TestHelpers.subscribe_users_to_club([logged_user], club)
        response = self.client.get('/users/%s/clubs/' % logged_user.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)