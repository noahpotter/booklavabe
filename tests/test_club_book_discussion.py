from rest_framework.test import APITestCase
from rest_framework import status
from tests.utils import TestHelpers
from architecture.models import ClubDiscussion
from guardian.shortcuts import get_perms, assign_perm

class AuthorizedActionTestCases(APITestCase):

    def test_president_create_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book({'president': logged_user})
        response = self.client.post('/club_books/%s/discussions/' % club_book.id, {'end_chapter': 1, 'start_chapter': 1}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_member_get_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], discussion)
        response = self.client.get('/club_book_discussions/%s/' % discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], discussion.id)

    def test_member_list_discussions(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion_1 = TestHelpers.create_club_book_discussion()
        discussion_2 = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], discussion_1)
        response = self.client.get('/club_books/%s/discussions/' % discussion_1.club_book.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], discussion_1.id)
        self.assertEqual(len(response.data), 1)

    def test_president_update_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion = TestHelpers.create_club_book_discussion({'president': logged_user})
        response = self.client.patch('/club_book_discussions/%s/' % discussion.id, {'end_chapter': 2, 'start_chapter': 1}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_president_delete_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion = TestHelpers.create_club_book_discussion({'president': logged_user})
        response = self.client.delete('/club_book_discussions/%s/' % discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class UnauthorizedActionTestCases(APITestCase):

    def test_member_create_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book()
        TestHelpers.subscribe_users_to_club([logged_user], club_book.club)
        response = self.client.post('/club_books/%s/discussions/' % club_book.id, {'end_chapter': 1, 'start_chapter': 1}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_create_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book()
        response = self.client.post('/club_books/%s/discussions/' % club_book.id, {'end_chapter': 1, 'start_chapter': 1}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_get_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        response = self.client.get('/club_book_discussions/%s/' % club.discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_non_member_list_discussions(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion_1 = TestHelpers.create_club_book_discussion()
        discussion_2 = TestHelpers.create_club_book_discussion()
        response = self.client.get('/club_books/%s/discussions/' % discussion_1.club_book.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_member_update_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.join_club({'user_id': logged_user.id, 'club_id': discussion.club.id})
        response = self.client.patch('/club_book_discussions/%s/' % discussion.id, {'start_chapter': 1, 'end_chapter': 2}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_update_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion = TestHelpers.create_club_book_discussion()
        response = self.client.patch('/club_book_discussions/%s/' % discussion.id, {'start_chapter': 1, 'end_chapter': 2}, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_member_delete_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.join_club({'user_id': logged_user.id, 'club_id': discussion.club.id})
        response = self.client.delete('/club_book_discussions/%s/' % discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_delete_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion = TestHelpers.create_club_book_discussion()
        response = self.client.delete('/club_book_discussions/%s/' % discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
