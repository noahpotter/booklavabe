from rest_framework.test import APITestCase
from rest_framework import status
from tests.utils import TestHelpers

class AuthorizedActionTestCases(APITestCase):

    def test_non_member_join_club(self):
        club = TestHelpers.create_club()
        user = TestHelpers.login_user(self)['user']
        response = self.client.post('/club_memberships/', {'club_id': club.id, 'user_id': user.id}, format='json')
        self.assertEqual(response.data['club']['id'], club.pk)
        self.assertEqual(response.data['user']['id'], user.pk)

    def test_create_club_membership_club_doesnt_exist_failure(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.post('/club_memberships/', {'club_id': 99999, 'user_id': user.id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_club_membership_already_president_failure(self):
        user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club({'president': user})
        response = self.client.post('/club_memberships/', {'club_id': club.id, 'user_id': user.id}, format='json')
        self.assertEqual(response.data['non_field_errors'], ['The fields club, user must make a unique set.'])

    def test_subscriber_unsubscribes(self):

        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.subscribe_users_to_club([logged_user], club)
        response = self.client.delete('/users/%s/clubs/%s/' % (logged_user.id, club.id), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(logged_user.subscribed_clubs), 0)

    def test_delete_owned_club_deletes_subscriptions(self):
        pass

class UnauthorizedActionTestCases(APITestCase):
    pass
