from tests.factories import (
    UserFactory, 
    ClubFactory, 
    BookFactory, 
    ClubBookFactory, 
    ClubBookDiscussionFactory, 
    ClubDiscussionFactory,
    ThreadFactory, 
    PostFactory,
    PostReportFactory
)
from django.contrib.auth.models import User
from architecture.models import Club, ClubDiscussion
from Booklava.serializers import (
    ClubSerializer, 
    UserSerializer, 
    BookSerializer, 
    ClubBookSerializer, 
    ClubMembershipSerializer, 
    ClubBookDiscussionSerializer, 
    ClubDiscussionSerializer, 
    ThreadSerializer, 
    PostSerializer,
    PostReportSerializer
)
import factory

class TestHelpers():
    @staticmethod
    def post_create_user(self):
        user = UserFactory.stub()
        response = self.client.post('/users/', {'username': user.username, 'email': user.email, 'password': user.password}, format='json')
        # user = TestHelpers.create_user({'username': user.username, 'email': user.email, 'password': user.password})
        return {'response': response, 'userOb': user, 'user': User.objects.last()}
        # return {'userOb': user, 'user': User.objects.last()}

    @staticmethod
    def login_user(self):
        result = TestHelpers.post_create_user(self)
        response = self.client.post('/api-token-auth/', {'email': result['userOb'].email, 'password': result['userOb'].password}, format='json')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + response.data['token'])
        return {'response': response, 'userOb': result['userOb'], 'user': result['user']}

    @staticmethod
    def create_user(data=None):
        if data is None:
            data = UserFactory.stub().__dict__
        else:
            data = UserFactory.stub(**data).__dict__

        user = UserSerializer(data=data)
        user.is_valid(raise_exception=True)
        user.save()
        return user.instance

    @staticmethod
    def create_club(data=None):
        if data is None:
            data = ClubFactory.stub().__dict__
        else:
            data = ClubFactory.stub(**data).__dict__

        club = ClubSerializer(data=data, context={'request': {'user': data['president']}})
        club.is_valid(raise_exception=True)
        data['president'].save()
        club.save()
        return club.instance

    @staticmethod
    def join_club(data): # user_id, club_id
        club_membership = ClubMembershipSerializer(data=data)
        club_membership.is_valid(raise_exception=True)
        club_membership.save()
        return club_membership.instance

    @staticmethod
    def create_book(data=None):
        if data is None:
            data = BookFactory.stub().__dict__
        else:
            data = BookFactory.stub(**data).__dict__

        if 'club_id' not in data:
            club = TestHelpers.create_club()
            data['club_id'] = club.id

        book = BookSerializer(data=data)
        book.is_valid(raise_exception=True)
        book.save()
        return book.instance

    @staticmethod
    def create_club_book(data=None):
        if data is None:
            club = TestHelpers.create_club()
            book = TestHelpers.create_book()
            data = ClubBookFactory.stub().__dict__
            data['club_id'] = club.id
            data['book_id'] = book.id
        else:
            if 'club_id' not in data:
                club = TestHelpers.create_club(data)
                data['club_id'] = club.id
            if 'book_id' not in data:
                book = TestHelpers.create_book(data)
                data['book_id'] = book.id

            data = ClubBookFactory.stub(**data).__dict__

        if 'book_id' not in data:
            book = TestHelpers.create_book()
            data['book_id'] = book.id

        club_book = ClubBookSerializer(data=data)
        club_book.is_valid(raise_exception=True)
        club_book.save()
        return club_book.instance

    @staticmethod
    def create_club_book_discussion(data=None):
        if data is None:
            data = ClubBookDiscussionFactory.stub().__dict__
        else:
            data = ClubBookDiscussionFactory.stub(**data).__dict__

        if 'club_book_id' not in data:
            club_book = TestHelpers.create_club_book(data)
            data['club_book_id'] = club_book.id

        club_book_discussion = ClubBookDiscussionSerializer(data=data)
        club_book_discussion.is_valid(raise_exception=True)
        club_book_discussion.save()
        return club_book_discussion.instance

    @staticmethod
    def create_club_discussion(data=None):
        if data is None:
            data = ClubDiscussionFactory.stub().__dict__
        else:
            data = ClubDiscussionFactory.stub(**data).__dict__

        club = TestHelpers.create_club()
        return club.discussion
        # if 'club_id' not in data:
        #     club = TestHelpers.create_club()
        #     data['club_id'] = club.id

        # club_discussion = ClubDiscussionSerializer(data=data)
        # club_discussion.is_valid(raise_exception=True)
        # club_discussion.save()
        # return club_discussion.instance

    @staticmethod
    def create_club_discussion_thread(data=None):
        if data is None:
            data = ThreadFactory.stub().__dict__
        else:
            data = ThreadFactory.stub(**data).__dict__

        if 'club_discussion_id' not in data:
            club_discussion = TestHelpers.create_club_discussion()
            data['club_discussion_id'] = club_discussion.id

        user = data.pop('user', None)

        thread = ThreadSerializer(data=data, context={'request': {'user': user}})
        thread.is_valid(raise_exception=True)
        thread.save()
        return thread.instance

    @staticmethod
    def create_club_book_discussion_thread(data=None):
        if data is None:
            data = ThreadFactory.stub().__dict__
        else:
            data = ThreadFactory.stub(**data).__dict__

        if 'club_book_discussion_id' not in data:
            club_book_discussion = TestHelpers.create_club_book_discussion()
            data['club_book_discussion_id'] = club_book_discussion.id

        user = data.pop('user', None)

        thread = ThreadSerializer(data=data, context={'request': {'user': user}})
        thread.is_valid(raise_exception=True)
        thread.save()
        return thread.instance

    @staticmethod
    def create_post(data=None):
        if data is None:
            data = PostFactory.stub().__dict__
        else:
            data = PostFactory.stub(**data).__dict__

        if 'thread_id' not in data:
            thread = TestHelpers.create_club_book_discussion_thread()
            data['thread_id'] = thread.id

        post = PostSerializer(data=data, context={'request': {'user': data['user']}})
        post.is_valid(raise_exception=True)
        post.save()
        return post.instance

    @staticmethod
    def create_post_report(data=None):
        if data is None:
            data = PostReportFactory.stub().__dict__
        else:
            data = PostReportFactory.stub(**data).__dict__

        if 'user_id' not in data:
            user = TestHelpers.create_user()
            data['user_id'] = user.id

        if 'post_id' not in data:
            post = TestHelpers.create_post()
            data['post_id'] = post.id

        post_report = PostReportSerializer(data=data)
        post_report.is_valid(raise_exception=True)
        post_report.save()
        return post_report.instance

    @staticmethod
    def subscribe_users_to_thread(users, thread):
        for user in users:
            club_membership = ClubMembershipSerializer(data={'user_id': user.id, 'club_id': thread.club.id})
            club_membership.is_valid(raise_exception=True)
            club_membership.save()

    @staticmethod
    def subscribe_users_to_discussion(users, discussion):
        for user in users:
            club_membership = ClubMembershipSerializer(data={'user_id': user.id, 'club_id': discussion.club.id})
            club_membership.is_valid(raise_exception=True)
            club_membership.save()

    @staticmethod
    def subscribe_users_to_club_book(users, club_book):
        for user in users:
            club_membership = ClubMembershipSerializer(data={'user_id': user.id, 'club_id': club_book.club.id})
            club_membership.is_valid(raise_exception=True)
            club_membership.save()

    @staticmethod
    def subscribe_users_to_club(users, club):
        for user in users:
            club_membership = ClubMembershipSerializer(data={'user_id': user.id, 'club_id': club.id})
            club_membership.is_valid(raise_exception=True)
            club_membership.save()