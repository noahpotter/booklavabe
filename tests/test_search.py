from rest_framework.test import APITestCase
from architecture.models import ClubMembership
from django.contrib.auth.models import User
from architecture.models import Club
from rest_framework import status
from tests.utils import TestHelpers

class AuthorizedActionTestCases(APITestCase):
    def test_search_users_exact_username(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.get('/search/', {'query': user.username, 'type': 'users'}, format='json')
        self.assertEqual(len(response.data), 1)

    def test_search_users_partial_username(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.get('/search/', {'query': user.username[0:3], 'type': 'users'}, format='json')
        self.assertEqual(len(response.data), 1)

    def test_search_users_no_match_failure(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.get('/search/', {'query': 'notfound', 'type': 'users'}, format='json')
        self.assertEqual(len(response.data), 0)

    def test_search_club_name(self):
        TestHelpers.login_user(self)
        TestHelpers.create_club({'name': 'test', 'description': 'description'})
        TestHelpers.create_club({'name': 'test', 'description': 'description'})
        TestHelpers.create_club({'name': 'club1', 'description': 'description'})
        response = self.client.get('/search/', {'query': 'test', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 2)

    def test_search_clubs_that_read_book(self):
        TestHelpers.login_user(self)
        book = TestHelpers.create_book({'name': 'giver'})
        club = TestHelpers.create_club()
        club_book = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book.id})
        response = self.client.get('/search/', {'query': 'book: giver', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 1)

    def test_search_clubs_that_read_book_with_multiple_words_in_name(self):
        TestHelpers.login_user(self)
        book = TestHelpers.create_book({'name': 'the giver'})
        club = TestHelpers.create_club()
        club_book = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book.id})
        response = self.client.get('/search/', {'query': 'book: the giver', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 1)

    def test_search_clubs_that_read_book_exact_match_failure(self):
        TestHelpers.login_user(self)
        book = TestHelpers.create_book({'name': 'the giver'})
        club = TestHelpers.create_club()
        club_book = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book.id})
        response = self.client.get('/search/', {'query': 'book: giver', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 0)

    def test_search_clubs_that_read_book_failure(self):
        TestHelpers.login_user(self)
        book = TestHelpers.create_book({'name': 'giver'})
        club = TestHelpers.create_club()
        club_book = TestHelpers.create_club_book({'club_id': club.id, 'book_id': book.id})
        response = self.client.get('/search/', {'query': 'book: nonexistant', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 0)

    def test_search_club_description(self):
        TestHelpers.login_user(self)
        TestHelpers.create_club({'name': 'club1', 'description': 'test description'})
        TestHelpers.create_club({'name': 'club2', 'description': 'test description'})
        TestHelpers.create_club({'name': 'club3', 'description': 'description'})
        response = self.client.get('/search/', {'query': 'test', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 2)

    def test_search_club_description_multiple_words(self):
        TestHelpers.login_user(self)
        TestHelpers.create_club({'name': 'club1', 'description': 'test1 description'})
        TestHelpers.create_club({'name': 'club2', 'description': 'test2 description'})
        response = self.client.get('/search/', {'query': 'test1 test2', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 2)

    # Difficult to test books now since gettings books from amazon
    # def test_search_book_name(self):
    #     TestHelpers.login_user(self)
    #     TestHelpers.create_book({'name': 'test'})
    #     TestHelpers.create_book({'name': 'book1'})
    #     response = self.client.get('/search/', {'query': 'Giver', 'type': 'books'}, format='json')
    #     print response.data
    #     self.assertEqual(len(response.data), 1)

    # def test_search_book_name_insensitive(self):
    #     TestHelpers.login_user(self)
    #     TestHelpers.create_book({'name': 'test'})
    #     TestHelpers.create_book({'name': 'book1'})
    #     response = self.client.get('/search/', {'query': 'TEST', 'type': 'books'}, format='json')
    #     self.assertEqual(len(response.data), 1)

    # def test_search_blank(self):
    #     TestHelpers.login_user(self)
    #     TestHelpers.create_book({'name': 'test'})
    #     TestHelpers.create_book({'name': 'book1'})
    #     response = self.client.get('/search/', {'query': '', 'type': 'books'}, format='json')
    #     self.assertEqual(len(response.data), 2)

    # def test_search_absent_word(self):
    #     TestHelpers.login_user(self)
    #     TestHelpers.create_book({'name': 'test'})
    #     TestHelpers.create_book({'name': 'book1'})
    #     response = self.client.get('/search/', {'query': 'nonexistant', 'type': 'books'}, format='json')
    #     self.assertEqual(len(response.data), 0)



class UnauthorizedActionTestCases(APITestCase):
    def test_anon_search_users_exact_username(self):
        user = TestHelpers.create_user()
        response = self.client.get('/search/', {'query': user.username, 'type': 'users'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
