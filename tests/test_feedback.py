from rest_framework.test import APITestCase
from rest_framework import status
from django.contrib.auth.models import User
from architecture.models import Club
from tests.utils import TestHelpers
from django.contrib.auth.hashers import (check_password, make_password)

import os

from django.core import mail
from django.test import TestCase

class AuthorizedActionTestCases(APITestCase):
    def test_user_create_feedback(self):
        logged_user = TestHelpers.login_user(self)['user']
        response = self.client.post('/feedback/', {'message': 'hello'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

class UnauthorizedActionTestCases(APITestCase):
    def test_non_user_create_feedback(self):
        self.client.logout()
        response = self.client.post('/feedback/', {'message': 'hello'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)