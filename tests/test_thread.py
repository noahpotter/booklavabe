from rest_framework.test import APITestCase
from architecture.models import (Thread, Post)
from rest_framework import status
from tests.utils import TestHelpers

class TestCases(APITestCase):

    def test_get_thread_returns_original_post(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_book_discussion)
        thread = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id, 'content': 'content', 'user': logged_user})
        post = thread.original_post
        response = self.client.get('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.data['original_post']['id'], post.id)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_discussion)
        thread = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id, 'content': 'content', 'user': logged_user})
        post = thread.original_post
        response = self.client.get('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.data['original_post']['id'], post.id)

class PartialGetTestCases(APITestCase):
    def test_getting_new_threads_first_time(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_book_discussion)
        thread_1 = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        thread_2 = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        response = self.client.get('/club_book_discussions/%s/threads/' % club_book_discussion.id, {'club_book_discussion_id': club_book_discussion.id, 'has_threads': []}, format='json')
        self.assertEqual(response.data[0]['id'], thread_1.id)
        self.assertEqual(response.data[1]['id'], thread_2.id)
        self.assertEqual(len(response.data), 2)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_discussion)
        thread_1 = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id})
        thread_2 = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id})
        response = self.client.get('/club_discussions/%s/threads/' % club_discussion.id, {'club_discussion_id': club_discussion.id, 'has_threads': []}, format='json')
        self.assertEqual(response.data[0]['id'], thread_1.id)
        self.assertEqual(response.data[1]['id'], thread_2.id)
        self.assertEqual(len(response.data), 2)

    def test_getting_new_threads(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_book_discussion)
        thread_1 = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        thread_2 = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        response = self.client.get('/club_book_discussions/%s/threads/' % club_book_discussion.id, {'has_threads': [thread_1.id]}, format='json')
        self.assertEqual(response.data[0]['id'], thread_2.id)
        self.assertEqual(len(response.data), 1)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_discussion)
        thread_1 = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id})
        thread_2 = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id})
        response = self.client.get('/club_discussions/%s/threads/' % club_discussion.id, {'has_threads': [thread_1.id]}, format='json')
        self.assertEqual(response.data[0]['id'], thread_2.id)
        self.assertEqual(len(response.data), 1)

    def test_getting_new_threads_already_updated(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_book_discussion)
        thread_1 = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        thread_2 = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        response = self.client.get('/club_book_discussions/%s/threads/' % club_book_discussion.id, {'club_book_discussion_id': club_book_discussion.id, 'has_threads': [thread_1.id, thread_2.id]}, format='json')
        self.assertEqual(len(response.data), 0)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_discussion)
        thread_1 = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id})
        thread_2 = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id})
        response = self.client.get('/club_discussions/%s/threads/' % club_discussion.id, {'club_discussion_id': club_discussion.id, 'has_threads': [thread_1.id, thread_2.id]}, format='json')
        self.assertEqual(len(response.data), 0)

class AuthorizedActionTestCases(APITestCase):

    def test_member_create_thread(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_book_discussion)
        response = self.client.post('/club_book_discussions/%s/threads/' % club_book_discussion.id, format='json')
        thread = Thread.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], thread.id)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_discussion)
        response = self.client.post('/club_discussions/%s/threads/' % club_discussion.id, format='json')
        thread = Thread.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], thread.id)

    def test_member_list_threads(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_book_discussion)
        thread_1 = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        thread_2 = TestHelpers.create_club_book_discussion_thread()
        response = self.client.get('/club_book_discussions/%s/threads/' % club_book_discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], thread_1.id)
        self.assertEqual(len(response.data), 1)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_discussion)
        thread_1 = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id})
        thread_2 = TestHelpers.create_club_discussion_thread()
        response = self.client.get('/club_discussions/%s/threads/' % club_discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], thread_1.id)
        self.assertEqual(len(response.data), 1)

    def test_member_create_thread_and_post(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_book_discussion)
        response = self.client.post('/club_book_discussions/%s/threads/' % club_book_discussion.id, {'content': 'custom content'}, format='json')
        thread = Thread.objects.last()
        post = Post.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], thread.id)
        self.assertEqual(post.content, 'custom content')
        self.assertEqual(post.thread, thread)
        self.assertEqual(logged_user.has_perm('view_post', post), True)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], club_discussion)
        response = self.client.post('/club_discussions/%s/threads/' % club_discussion.id, {'content': 'custom content'}, format='json')
        thread = Thread.objects.last()
        post = Post.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], thread.id)
        self.assertEqual(post.content, 'custom content')
        self.assertEqual(post.thread, thread)
        self.assertEqual(logged_user.has_perm('view_post', post), True)

    def test_member_get_thread(self):
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        response = self.client.get('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], thread.id)

        thread = TestHelpers.create_club_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        response = self.client.get('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], thread.id)

class UnauthorizedActionTestCases(APITestCase):

    def test_non_member_create_thread(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([user], club_book_discussion)
        response = self.client.post('/club_book_discussions/%s/threads/' % club_book_discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([user], club_discussion)
        response = self.client.post('/club_discussions/%s/threads/' % club_discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_member_creating_thread_for_nonexistant_discussion(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([user], club_book_discussion)
        response = self.client.post('/club_book_discussions/%s/threads/' % 999999, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([user], club_discussion)
        response = self.client.post('/club_discussions/%s/threads/' % 999999, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_non_member_list_threads(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([user], club_book_discussion)
        thread_1 = TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        thread_2 = TestHelpers.create_club_book_discussion_thread()
        response = self.client.get('/club_book_discussions/%s/threads/' % club_book_discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([user], club_discussion)
        thread_1 = TestHelpers.create_club_discussion_thread({'club_discussion_id': club_discussion.id})
        thread_2 = TestHelpers.create_club_discussion_thread()
        response = self.client.get('/club_discussions/%s/threads/' % club_discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_non_member_create_thread_and_post(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        club_book_discussion = TestHelpers.create_club_book_discussion()
        TestHelpers.subscribe_users_to_discussion([user], club_book_discussion)
        response = self.client.post('/club_book_discussions/%s/threads/' % club_book_discussion.id, {'content': 'custom content'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        club_discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([user], club_discussion)
        response = self.client.post('/club_discussions/%s/threads/' % club_discussion.id, {'content': 'custom content'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_non_member_get_thread(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user], thread)
        response = self.client.get('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        thread = TestHelpers.create_club_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user], thread)
        response = self.client.get('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_member_update_thread(self):
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        response = self.client.patch('/threads/%s/' % thread.id, {'content': 'new content'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        thread = TestHelpers.create_club_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        response = self.client.patch('/threads/%s/' % thread.id, {'content': 'new content'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_member_delete_thread(self):
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        response = self.client.delete('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        thread = TestHelpers.create_club_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        response = self.client.delete('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

