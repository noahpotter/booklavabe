from rest_framework.test import APITestCase
from rest_framework import status
from django.contrib.auth.models import User
from architecture.models import Club
from tests.utils import TestHelpers
from django.contrib.auth.hashers import (check_password, make_password)

import os

from django.core import mail
from django.test import TestCase

class TestCases(APITestCase):
  def test_edit_user_detail_success(self):
    file_path = os.path.join(os.path.dirname(__file__), 'files/image.png')
    with open(file_path) as fp:
        result = TestHelpers.login_user(self)
        user = result['user']
        userOb = result['userOb']
        response = self.client.patch('/user_details/%s/' % user.user_detail.id, {'image': fp, 'password': userOb.password}, format='multipart')
        self.assertNotEqual(response.data['image'], '')
        self.assertEqual(response.data['id'], user.user_detail.id)
