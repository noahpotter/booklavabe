# from rest_framework.test import APITestCase
# from architecture.models import ClubMembership
# from django.contrib.auth.models import User
# from architecture.models import Book
# from rest_framework import status
# from tests.utils import TestHelpers
# from Booklava.serializers import ClubMembershipSerializer

# class TestCases(APITestCase):
#     def test_(self):
#         userOb = TestHelpers.login_user(self)['user']
#         user = User.objects.get(username=userOb.username)
#         club = TestHelpers.create_club(user)
#         book = TestHelpers.create_book()
#         response = self.client.post('/clubbooks/', {'book_id': book.id, 'club_id': club.id, 'end_date': '2015-05-25T00:00' }, format='json')
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#         self.assertEqual(response.data['book']['id'], book.id)
#         self.assertEqual(response.data['club']['id'], club.id)

#     def test_get_clubbook_success(self):
#         result = TestHelpers.create_clubbook()
#         clubbook = result['clubbook']
#         book = result['book']
#         club = result['club']
#         response = self.client.get('/clubbooks/%s/' % (clubbook.id), format='json')
#         self.assertEqual(response.data['book']['id'], book.id)
#         self.assertEqual(response.data['club']['id'], club.id)

#     def test_non_president_create_clubbook_failure(self):
#         pass