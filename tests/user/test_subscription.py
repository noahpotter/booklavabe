from rest_framework.test import APITestCase
from architecture.models import Subscription
from django.contrib.auth.models import User
from rest_framework import status
from tests.utils import TestHelpers

class SubscriptionTest(APITestCase):

    def test_valid_subscriptions(self):
        Subscription.create_subscriptions()
        self.assertEqual(len(Subscription.objects.all()), 3)

    def test_default_user_subscription(self):
        Subscription.create_subscriptions()
        user = TestHelpers.create_user()
        self.assertEqual(user.user_subscription.subscription.name, 'default')

    def test_default_user_subscription_owned_clubs_below_limit(self):
        Subscription.create_subscriptions()
        user = TestHelpers.login_user(self)['user']
        TestHelpers.create_club({'president': user})

        response = self.client.post('/users/%s/clubs/' % user.id, {'name': 'new_name'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_default_user_subscription_owned_clubs_above_limit(self):
        Subscription.create_subscriptions()
        user = TestHelpers.login_user(self)['user']
        TestHelpers.create_club({'president': user})
        TestHelpers.create_club({'president': user})

        response = self.client.post('/users/%s/clubs/' % user.id, {'name': 'new_name'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_default_user_subscription_subscribed_clubs_doesnt_use_owned_clubs(self):
        Subscription.create_subscriptions()
        user = TestHelpers.login_user(self)['user']
        TestHelpers.create_club({'president': user})
        TestHelpers.create_club({'president': user})
        club1 = TestHelpers.create_club()
        club2 = TestHelpers.create_club()

        TestHelpers.join_club({'club_id': club1.id, 'user_id': user.id})

        response = self.client.post('/club_memberships/', {'user_id': user.id, 'club_id': club2.id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_default_user_subscription_subscribed_clubs_below_limit(self):
        Subscription.create_subscriptions()
        user = TestHelpers.login_user(self)['user']
        club1 = TestHelpers.create_club()
        club2 = TestHelpers.create_club()
        club3 = TestHelpers.create_club()

        TestHelpers.join_club({'club_id': club1.id, 'user_id': user.id})
        TestHelpers.join_club({'club_id': club2.id, 'user_id': user.id})

        response = self.client.post('/club_memberships/', {'user_id': user.id, 'club_id': club3.id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_default_user_subscription_subscribed_clubs_above_limit(self):
        Subscription.create_subscriptions()
        user = TestHelpers.login_user(self)['user']
        club1 = TestHelpers.create_club()
        club2 = TestHelpers.create_club()
        club3 = TestHelpers.create_club()
        club4 = TestHelpers.create_club()

        TestHelpers.join_club({'club_id': club1.id, 'user_id': user.id})
        TestHelpers.join_club({'club_id': club2.id, 'user_id': user.id})
        TestHelpers.join_club({'club_id': club3.id, 'user_id': user.id})

        response = self.client.post('/club_memberships/', {'user_id': user.id, 'club_id': club4.id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


