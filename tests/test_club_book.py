from rest_framework.test import APITestCase
from architecture.models import ClubMembership
from django.contrib.auth.models import User
from architecture.models import Club
from rest_framework import status
from tests.utils import TestHelpers
from Booklava.serializers import ClubMembershipSerializer

class AuthorizedActionTestCases(APITestCase):
    def test_president_create_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club({'president': logged_user})
        book = TestHelpers.create_book()
        response = self.client.post('/clubs/%s/club_books/' % club.id, {'book_id': book.id, 'start_date':  '2015-05-24T00:00', 'end_date': '2015-05-25T00:00' }, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_member_get_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book()
        TestHelpers.subscribe_users_to_club_book([logged_user], club_book)
        response = self.client.get('/club_books/%s/' % (club_book.id), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_non_member_get_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book()
        response = self.client.get('/club_books/%s/' % (club_book.id), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_member_list_club_books(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_1 = TestHelpers.create_club_book()
        club_book_2 = TestHelpers.create_club_book()
        TestHelpers.subscribe_users_to_club_book([logged_user], club_book_1)
        TestHelpers.subscribe_users_to_club_book([logged_user], club_book_2)
        response = self.client.get('/clubs/%s/club_books/' % club_book_1.club.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], club_book_1.id)
        self.assertEqual(len(response.data), 1)

    def test_non_member_list_club_books(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book_1 = TestHelpers.create_club_book()
        club_book_2 = TestHelpers.create_club_book()
        response = self.client.get('/clubs/%s/club_books/' % club_book_1.club.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], club_book_1.id)
        self.assertEqual(len(response.data), 1)

    def test_president_update_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book({'president': logged_user})
        response = self.client.patch('/club_books/%s/' % club_book.id, {'start_date': '2016-05-25T00:00', 'end_date': '2016-05-26T00:00'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_president_delete_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book({'president': logged_user})
        response = self.client.delete('/club_books/%s/' % club_book.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_president_delete_club_book_with_threads(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book({'president': logged_user})
        club_book_discussion = TestHelpers.create_club_book_discussion({'club_book_id': club_book.id})
        TestHelpers.create_club_book_discussion_thread({'club_book_discussion_id': club_book_discussion.id})
        print club_book.discussions.count()
        print club_book.discussions.first
        response = self.client.delete('/club_books/%s/' % club_book.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class UnauthorizedActionTestCases(APITestCase):

    def test_member_create_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        book = TestHelpers.create_book()
        TestHelpers.subscribe_users_to_club([logged_user], club)
        response = self.client.post('/clubs/%s/club_books/' % club.id, {'book_id': book.id, 'start_date':  '2015-05-24T00:00', 'end_date': '2015-05-25T00:00' }, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_create_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        book = TestHelpers.create_book()
        response = self.client.post('/clubs/%s/club_books/' % club.id, {'book_id': book.id, 'start_date':  '2015-05-24T00:00', 'end_date': '2015-05-25T00:00' }, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_member_update_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book()
        TestHelpers.subscribe_users_to_club_book([logged_user], club_book)
        response = self.client.patch('/club_books/%s/' % club_book.id, {'start_date': '2016-05-25T00:00', 'end_date': '2016-05-26T00:00'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_update_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book()
        response = self.client.patch('/club_books/%s/' % club_book.id, {'start_date': '2016-05-25T00:00', 'end_date': '2016-05-26T00:00'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_member_delete_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book()
        TestHelpers.subscribe_users_to_club_book([logged_user], club_book)
        response = self.client.delete('/club_books/%s/' % club_book.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_delete_club_book(self):
        logged_user = TestHelpers.login_user(self)['user']
        club_book = TestHelpers.create_club_book()
        response = self.client.delete('/club_books/%s/' % club_book.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)