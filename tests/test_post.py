from rest_framework.test import APITestCase
from architecture.models import Post, PostReport
from rest_framework import status
from tests.utils import TestHelpers
from guardian.shortcuts import get_perms, assign_perm

class CreatingPostTestCases(APITestCase):
    def test_editing_post_keeps_nested_level(self):
        user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        post_1 = TestHelpers.create_post({'thread_id': thread.id, 'user': user})
        post_2 = TestHelpers.create_post({'thread_id': thread.id, 'user': user, 'parent_post_id': post_1.id})
        new_content = 'new content'
        response = self.client.patch('/posts/%s/' % post_2.id, {'content': new_content}, format='json')
        self.assertEqual(response.data['nested_level'], 1)

class PartialGetTestCases(APITestCase):
    def test_getting_new_posts_first_time(self):
        user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user], thread)
        post_1 = TestHelpers.create_post({'thread_id': thread.id, 'user': user})
        post_2 = TestHelpers.create_post({'thread_id': thread.id, 'user': user, 'parent_post_id': post_1.id})
        response = self.client.get('/threads/%s/posts/' % thread.id, {'has_posts': []}, format='json')
        self.assertEqual(response.data[0]['id'], post_1.id)
        self.assertEqual(response.data[1]['id'], post_2.id)
        self.assertEqual(len(response.data), 2)

    def test_getting_new_posts(self):
        user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user], thread)
        post_1 = TestHelpers.create_post({'thread_id': thread.id, 'user': user})
        post_2 = TestHelpers.create_post({'thread_id': thread.id, 'user': user, 'parent_post_id': post_1.id})
        response = self.client.get('/threads/%s/posts/' % thread.id, {'has_posts': [post_1.id]}, format='json')
        self.assertEqual(response.data[0]['id'], post_2.id)
        self.assertEqual(len(response.data), 1)

    def test_getting_new_posts_already_updated(self):
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        post_1 = TestHelpers.create_post({'thread_id': thread.id, 'user': logged_user})
        post_2 = TestHelpers.create_post({'thread_id': thread.id, 'user': logged_user, 'parent_post_id': post_1.id})
        response = self.client.get('/threads/%s/posts/' % thread.id, {'has_posts': [post_1.id, post_2.id]}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

class DeletedPostTestCases(APITestCase):
    def test_getting_a_deleted_post(self):
        user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user})
        response = self.client.delete('/posts/%s/' % post.id, format='json')
        response = self.client.get('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], post.id)
        self.assertEqual(response.data['content'], '')
        self.assertEqual(response.data['deleted'], True)

    def test_getting_a_non_deleted_post(self):
        user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user})
        response = self.client.get('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], post.id)
        self.assertEqual(response.data['content'], post.content)
        self.assertEqual(response.data['deleted'], False)

    def test_deleting_a_post(self):
        user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user})
        response = self.client.delete('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], post.id)
        self.assertEqual(response.data['content'], '')
        self.assertEqual(response.data['deleted'], True)

class ReportingTestCases(APITestCase):

    def test_reporting_a_post_requires_more(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1, logged_user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.post('/posts/%s/report/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], post.id)
        self.assertEqual(response.data['content'], post.content)
        self.assertEqual(response.data['deleted'], False)

    def test_reporting_a_post_has_enough(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1, logged_user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        TestHelpers.create_post_report({'post_id': post.id})
        TestHelpers.create_post_report({'post_id': post.id})
        TestHelpers.create_post_report({'post_id': post.id})
        TestHelpers.create_post_report({'post_id': post.id})
        TestHelpers.create_post_report({'post_id': post.id})
        response = self.client.post('/posts/%s/report/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], post.id)
        self.assertEqual(response.data['content'], '')
        self.assertEqual(response.data['deleted'], True)

class AuthorizedActionTestCases(APITestCase):

    def test_member_creating_post(self):
        user_1 = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        response = self.client.post('/threads/%s/posts/' % thread.id, {'thread_id': thread.id, 'content': 'content'},   format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_creating_member_updating_post(self):
        user_1 = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.patch('/posts/%s/' % post.id, {'content': 'new content'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_club_member_get_post(self):
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': logged_user})
        response = self.client.get('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], post.id)
        self.assertEqual(response.data['thread'], thread.id)
        self.assertEqual(response.data['user'], logged_user.id)

    def test_club_member_get_posts_in_thread(self):
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        post_1 = TestHelpers.create_post({'thread_id': thread.id, 'user': logged_user})
        post_2 = TestHelpers.create_post({'user': logged_user})
        response = self.client.get('/threads/%s/posts/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], post_1.id)
        self.assertEqual(len(response.data), 1)

    def test_creating_member_deleting_post(self):
        user_1 = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.delete('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_strange_member_reporting_a_post(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.post('/posts/%s/report/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class UnauthorizedActionTestCases(APITestCase):

    def test_non_member_creating_post(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        response = self.client.post('/threads/%s/posts/' % thread.id, {'thread_id': thread.id, 'content': 'content'},   format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_strange_member_updating_post(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.patch('/posts/%s/' % post.id, {'content': 'new content'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_updating_post(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.patch('/posts/%s/' % post.id, {'content': 'new content'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_non_member_get_post(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.get('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_non_member_get_posts_in_thread(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        post_1 = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        post_2 = TestHelpers.create_post({'user': user_1})
        response = self.client.get('/threads/%s/posts/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_strange_member_deleting_post(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1, logged_user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.delete('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_deleting_post(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.delete('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_creating_member_reporting_a_post(self):
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([logged_user], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': logged_user})
        response = self.client.post('/posts/%s/report/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_reporting_a_post(self):
        user_1 = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        thread = TestHelpers.create_club_book_discussion_thread()
        TestHelpers.subscribe_users_to_thread([user_1], thread)
        post = TestHelpers.create_post({'thread_id': thread.id, 'user': user_1})
        response = self.client.post('/posts/%s/report/' % post.id, format='json')
        # 404 Because the logged user shouldn't even be able to see that this post exists
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
