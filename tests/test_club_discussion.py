from rest_framework.test import APITestCase
from rest_framework import status
from tests.utils import TestHelpers
from architecture.models import ClubDiscussion
from guardian.shortcuts import get_perms, assign_perm

class AuthorizedActionTestCases(APITestCase):

    def test_member_get_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], discussion)
        response = self.client.get('/club_discussions/%s/' % discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], discussion.id)

    def test_list_discussions(self):
        logged_user = TestHelpers.login_user(self)['user']
        discussion_1 = TestHelpers.create_club_discussion()
        discussion_2 = TestHelpers.create_club_discussion()
        TestHelpers.subscribe_users_to_discussion([logged_user], discussion_1)
        response = self.client.get('/clubs/%s/discussions/' % discussion_1.club.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], discussion_1.id)
        self.assertEqual(len(response.data), 1)

class UnauthorizedActionTestCases(APITestCase):

    def test_member_create_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.join_club({'user_id': logged_user.id, 'club_id': club.id})
        response = self.client.post('/clubs/%s/discussions/' % club.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_create_discussion(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.join_club({'user_id': user.id, 'club_id': club.id})
        response = self.client.post('/clubs/%s/discussions/' % club.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_get_discussion(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.join_club({'user_id': user.id, 'club_id': club.id})
        response = self.client.get('/club_discussions/%s/' % club.discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_member_updating_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.join_club({'user_id': logged_user.id, 'club_id': club.id})
        response = self.client.patch('/club_discussions/%s/' % club.discussion.id, {'date': 'new date'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_updating_discussion(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.join_club({'user_id': user.id, 'club_id': club.id})
        response = self.client.patch('/club_discussions/%s/' % club.discussion.id, {'date': 'new date'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_member_deleting_discussion(self):
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.join_club({'user_id': logged_user.id, 'club_id': club.id})
        response = self.client.delete('/club_discussions/%s/' % club.discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_non_member_deleting_discussion(self):
        user = TestHelpers.create_user()
        logged_user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club()
        TestHelpers.join_club({'user_id': user.id, 'club_id': club.id})
        response = self.client.delete('/club_discussions/%s/' % club.discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
